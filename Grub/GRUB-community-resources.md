---
title: GRUB community resources
---

Where to find information, or ask questions? Updated in April 2018.

##### Contents
<!-- MarkdownTOC levels="1,2" autolink="true" -->

- [Articles](#articles)
- [Resources maintained by Grub project](#resources-maintained-by-grub-project)
	- [Manual](#manual)
	- [Mailing lists](#mailing-lists)
- [Presentations](#presentations)
- [Media presence](#media-presence)
- [Forum posts with valuable feedback](#forum-posts-with-valuable-feedback)

<!-- /MarkdownTOC -->



# Articles
- [How does GNU GRUB work](https://0xax.github.io/grub/) an excellent in-depth article by [0xAX](https://twitter.com/0xAX).



# Resources maintained by Grub project

## Manual
- [Separate pages, fast loading](https://www.gnu.org/software/grub/manual/grub/html_node/index.html)
- [On one page, easy to search](https://www.gnu.org/software/grub/manual/grub/grub.html).
- There should be an open knowledgebase (aka. wiki) to document all the new features and use-cases of Grub. The wiki can serve as a place to bring together downstream developers, improve cooperation, help upstreaming patches, features.

## Mailing lists
Main Grub forum is the mailing lists at gnu.org. This suits the developers mostly.
- [[Grub-devel]](http://grub-devel.gnu.narkive.com/)  --  [[at Gnu]](http://lists.gnu.org/archive/html/grub-devel/) [[Subscribe]](https://lists.gnu.org/mailman/listinfo/grub-devel)
- [[Bug-grub]](http://bug-grub.gnu.narkive.com/)  [[at Gnu]](http://lists.gnu.org/archive/html/bug-grub/) [[Subscribe]](https://lists.gnu.org/mailman/listinfo/bug-grub)
- [[Help-grub]](http://help-grub.gnu.narkive.com/)  [[at Gnu]](http://lists.gnu.org/archive/html/help-grub/) [[Subscribe]](https://lists.gnu.org/mailman/listinfo/help-grub)
- [[Bug tracker]](http://savannah.gnu.org/bugs/?group=grub)

### Themes, wiki, forum
- A repository-backed theme registry would be an attractive service, increasing user involvement and satisfaction.
- Many tips are scattered around on distribution wikis. A community wiki, collecting guides and best practices in use could connect all these pieces. These tips include how to use different bootmanagers, therefore it might be separate from the Grub documentation wiki.
- A Discourse forum would be more accessible for users, who don't follow the mailing list on a daily basis. It is easier to search and browse than the mailing lists.



# Presentations

### 4th Feb 2018 - [GRUB upstream and distros cooperation](https://fosdem.org/2018/schedule/event/grub_upstream_and_distros_cooperation/)
- at FOSDEM18 Brussels
- Daniel Kiper, Vladimir 'phcoder' Serbinenko
- [slides](https://fosdem.org/2018/schedule/event/grub_upstream_and_distros_cooperation/attachments/slides/2399/export/events/attachments/grub_upstream_and_distros_cooperation/slides/2399/grub_upstream_and_distros_cooperation_20180203_final_dk_vs.pdf), video: [mp4](https://video.fosdem.org/2018/K.3.201/grub_upstream_and_distros_cooperation.mp4), [webm](https://video.fosdem.org/2018/K.3.201/grub_upstream_and_distros_cooperation.webm)

### 5th Feb 2017 - [New maintainers, news and future](https://archive.fosdem.org/2017/schedule/event/grub_new_maintainers/)
- at FOSDEM17 Brussels
- Vladimir 'phcoder' Serbinenko, Daniel Kiper, Maria Komar
- [slides](https://archive.fosdem.org/2017/schedule/event/grub_new_maintainers/attachments/slides/1768/export/events/attachments/grub_new_maintainers/slides/1768/slides.pdf), video: [mp4](https://video.fosdem.org/2017/H.2215/grub_new_maintainers.mp4), [webm](https://video.fosdem.org/2017/H.2215/grub_new_maintainers.vp8.webm)



# Media presence

#### 2018
- 11 April 2018 - https://www.phoronix.com/scan.php?page=news_item&px=GRUB-Now-Supports-F2FS
- 14 March 2018 - https://www.phoronix.com/scan.php?page=news_item&px=GRUB-Multiple-Early-Initrd
- 4 February 2018 - https://www.phoronix.com/scan.php?page=news_item&px=GRUB-2.04-Second-Half-2018

#### 2017
- 29 June 2017 - https://www.phoronix.com/scan.php?page=news_item&px=GRUB-EXT4-Partial-Encrypt
- 3 May 2017 - https://www.phoronix.com/scan.php?page=news_item&px=GRUB-2.03-Development
- 25 April 2017 - https://www.phoronix.com/scan.php?page=news_item&px=GRUB-2.02-Tagged
- 16 February 2017 - https://www.phoronix.com/scan.php?page=news_item&px=GRUB-2.02-RC1-Features

#### 2016
- 13 March 2016 - https://www.phoronix.com/scan.php?page=news_item&px=GRUB-2.02-Features



# Forum posts with valuable feedback

#### 2018
- 2018-04-11 - https://www.phoronix.com/forums/forum/software/general-linux-open-source/1018302-grub-boot-loader-picks-up-support-for-f2fs-file-system

#### 2017
- 2017-04-25 - https://www.phoronix.com/forums/forum/software/general-linux-open-source/948379-grub-2-02-is-ready-to-boot-your-system?view=stream
- 2017-02-16 - https://www.phoronix.com/forums/forum/software/general-linux-open-source/932514-grub-2-02-is-still-coming-along-with-many-features-even-morse-code-output
  - gfxterm performace issue: cursor movement close to 1 sec

#### 2016
- 2016-03-13 - https://www.phoronix.com/forums/forum/software/general-linux-open-source/857983-there-are-many-features-coming-for-grub-2-02



