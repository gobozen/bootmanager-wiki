---
title: What type of kernel GRUB has
---

Grub (v1 and 2 too) is comparable to a [Unikernel](https://en.wikipedia.org/wiki/Unikernel): it runs only in Ring 0, and there is no userspace implemented.
It's also similar to [Monolithic kernel](https://en.wikipedia.org/wiki/Monolithic_kernel)s: modules (hardware, network and filesystem drivers, the OS loaders and even the Grub menu) are loaded to the same address space as the kernel, like modules in the Linux Kernel. Note that Grub was not designed as an Operating system, but a bootloader, that needed a few operating system features to be able to accomplish its task, therefore it has no "official" categorization.

To distinguishing common kernel types:
- [Microkernel](https://en.wikipedia.org/wiki/Microkernel)s ([Minix](https://en.wikipedia.org/wiki/Minix), [Redox](https://en.wikipedia.org/wiki/Redox)) run their drivers with less privileges in [Protection ring](https://en.wikipedia.org/wiki/Protection_ring) 1-3, than the kernel itself in Ring 0, and the memory area (address space) they can write to is separated, thereby protecting the kernel from driver anomalies.
- [Monolithic kernel](https://en.wikipedia.org/wiki/Monolithic_kernel)s (Linux, Windows) run drivers in the most privileged Ring 0 and separate user programs in Ring 3.
- [Unikernel](https://en.wikipedia.org/wiki/Unikernel)s (aka. [Library OS](https://en.wikipedia.org/wiki/Single_address_space_operating_system)es) ([IncludeOS](https://en.wikipedia.org/wiki/IncludeOS), [OSv](https://en.wikipedia.org/wiki/OSv), [MirageOS](https://mirage.io/) run even user programs in the same memory address space and Protection ring as the kernel (Ring 0, but [this might change in the future](http://unikernel.org/blog/2017/unikernels-are-secure)).

