---
title: GRUB distribution patches
---

How distributions use Grub, what patches are applied?
<br>This page collects references to distributions' usage and build of Grub:
- wiki pages showing typical usage patterns, recurring issues
- tracks significant repositories with patches, build recipes, forks

##### Contents
<!-- MarkdownTOC levels="1,2" autolink="true" -->

- [Upstream](#upstream)
- [Debian](#debian)
- [Ubuntu](#ubuntu)
- [Arch](#arch)
- [Antergos](#antergos)
- [Gentoo](#gentoo)
- [Fedora](#fedora)
- [RedHat](#redhat)
- [CentOS](#centos)
- [CoreOS](#coreos)

<!-- /MarkdownTOC -->



## Upstream
- https://gitlab.com/GNU/GRUB
- http://git.savannah.gnu.org/cgit/grub.git/tree/



## Debian
- https://wiki.debian.org/Grub2
- a lot of patches, all debian, ubuntu derivatives benefit:
- git: https://salsa.debian.org/grub-team/grub/tree/master/debian/patches
- moved from: https://anonscm.debian.org/cgit/pkg-grub/grub.git/tree/debian/patches
- patch tracker: https://sources.debian.org/src/grub2/2.02+dfsg1-4/debian/patches/series/
- patch tracker: https://sources.debian.org/patches/grub2/2.02+dfsg1-4/


	
## Ubuntu
- https://help.ubuntu.com/community/Grub2
- https://launchpad.net/ubuntu/+source/grub2
- https://launchpad.net/ubuntu/+source/grub2-signed



## Arch
- wiki: https://wiki.archlinux.org/index.php/GRUB
- package: https://www.archlinux.org/packages/core/x86_64/grub/
- patches: https://git.archlinux.org/svntogit/packages.git/tree/trunk?h=packages/grub



## Antergos
- https://github.com/manuel-192/antergos-customcfg
- Generates grub boot menu entries into file /boot/grub/custom.cfg



## Gentoo
https://wiki.gentoo.org/wiki/GRUB2/en


## Fedora
https://fedoraproject.org/wiki/GRUB_2


## RedHat
https://github.com/rhboot/grub2


## CentOS
https://wiki.centos.org/HowTos/Grub2


## CoreOS
https://github.com/coreos/grub/commits/2.02-coreos


