---
title: GRUB boot sequence visualized
---

## Grub 2 components and boot sequence on MBR vs. GPT partitioned hard drive

![https://commons.wikimedia.org/w/index.php?curid=28215775](GNU_GRUB_components.svg "Grub 2 startup components")

  Image by Shmuel Csaba Otto Traian, CC BY-SA 3.0, https://commons.wikimedia.org/w/index.php?curid=28215775


Grub2 has very similar stages as Grub v1. Some parts were moved around, but the assembler code of early stages is mostly the same. While Grub v1 had a documented process of
* `Stage 1 -> Stage 1.5 sector 0 -> Stage 1.5 remaining sectors -> Stage 2 -> loading config`, Grub2's process is not documented in such detail. Reading the code reveals the Grub v1 stages can be mapped roughly to:
* `MBR (boot.img) -> core.img sector 0 -> core.img remaining sectors -> normal.mod -> loading config, additional modules`

Grub v1 and 2 also has 4 stages, but named in a different way. The stages are different in the way they load the next stage. 1st stage looks for 2nd stage in *one* fixed LBA sector. 2nd stage loads 3rd from *more* fixed LBA sectors (called a blocklist in Grub documentation, or extents in recent filesystems). 3rd stage loads 4th, and the config from a file record identified by partition number and file name.

The 1st and 2nd stages are necessary because the Legacy BIOS boot process has too little space (one sector) for a filesystem driver written in C. Even the blocklist of the 3rd stage did not fit in the 1st stage, making 2nd stage necessary. These stages expect that the next stage is not moved on the disk. If it's relocated, then the boot will fail without error message printed. This is why Grub2 discourages the use of blocklists in partitions, where defragmenting can move blocks. 3rd stage has just enough space for the necessary filesystem driver(s) to load modules and the config. Note that 3rd stage can also fail if the partition's index containing the modules is changed. This happens unexpectedly if it's a logical partition and another logical partition before it is removed.

This process is much simpler and safer on UEFI systems: `UEFI -> /EFI/<distro>/grubx64.efi (or similar, this used to be core.img) -> config`. No need for the crammed and fragile early stages.



## Grub2 components on MBR-partitioned hard drive

![https://commons.wikimedia.org/w/index.php?curid=28427221](GNU_GRUB_on_MBR_partitioned_hard_disk_drives.svg "Grub Legacy startup components, MBR partitioning")

  Image by Shmuel Csaba Otto Traian, CC BY-SA 3.0, https://commons.wikimedia.org/w/index.php?curid=28427221



## Grub2 components on GPT-partitioned hard drive

![https://commons.wikimedia.org/w/index.php?curid=28427209](GNU_GRUB_on_GPT_partitioned_hard_disk_drives.svg "Grub Legacy startup components, GPT partitioning")

  Image by Shmuel Csaba Otto Traian, CC BY-SA 3.0, https://commons.wikimedia.org/w/index.php?curid=28427209

