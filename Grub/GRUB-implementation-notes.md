---
title: GRUB implementation notes
---

##### Contents
<!-- MarkdownTOC levels="1,2" autolink="true" -->

- [Calculating $prefix](#calculating-%24prefix)

<!-- /MarkdownTOC -->



## Calculating $prefix

```
$cmdpath = firmware provided (fwdevice)/fwpath

module[OBJ_TYPE_PREFIX] = mkimage set [(device)][/path]
device == [drive][,part]
device = [drive||fwdrive],part
fwpath: cut suffix "/"*, "i386-pc"

$root = device
$prefix = (device)/path
```

