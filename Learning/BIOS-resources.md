---
title: BIOS resources
---

A trip down memory lane.

##### Contents
<!-- MarkdownTOC levels="1,2" autolink="true" -->

- [BIOS memory](#bios-memory)
- [Int 12h](#int-12h)
- [Int 13h](#int-13h)
- [Int 16h](#int-16h)
- [Int 18h](#int-18h)
- [Int 19h](#int-19h)

<!-- /MarkdownTOC -->



## BIOS memory
- http://webpages.charter.net/danrollins/techhelp/0093.HTM
	- ROM-BIOS Variables
- http://webpages.charter.net/danrollins/techhelp/0094.HTM
	- General Memory Map
- https://files.osdev.org/mirrors/geezer/osd/ram/index.htm#layout
	- Physical memory layout of the PC
- http://www.bioscentral.com/misc/bda.htm
	- BIOS Data Area


## Int 12h
- Get available conventional memory = bottom of Extended BIOS Data Area.
- Returns 40h:0013h in the BIOS Data Area by default.
- http://webpages.charter.net/danrollins/techhelp/0184.HTM


## Int 13h
- https://en.wikipedia.org/wiki/INT_13H#INT_13h_AH=08h:_Read_Drive_Parameters
- https://en.wikipedia.org/wiki/INT_13H#INT_13h_AH=41h:_Check_Extensions_Present
- https://en.wikipedia.org/wiki/INT_13H#INT_13h_AH=42h:_Extended_Read_Sectors_From_Drive
- https://wiki.osdev.org/ATA_in_x86_RealMode_(BIOS)#LBA_in_Extended_Mode
	- All systems support CHS addressing.
	- There exist some 486 systems that do not support LBA in any way. All known Pentium systems support Extended LBA in the BIOS.
- http://web.inter.nl.net/hcc/J.Steunebrink/bioslim.htm - The BIOS IDE Harddisk Limitations

#### Ralf Brown's
- http://www.ctyme.com/intr/int-13.htm
- http://www.ctyme.com/intr/rb-0715.htm
  - Int 13/AH=48h - get drive parameters
- http://stanislavs.org/helppc/int_13.html

#### Specification
- http://mbldr.sourceforge.net/specsedd30.pdf
	- Enhanced Disk Drive Specification - Version 3.0 - Rev 0.8 - March 12, 1998


## Int 16h
- Wait for Keypress and Read Character
- http://stanislavs.org/helppc/int_16-0.html
- http://webpages.charter.net/danrollins/techhelp/0229.HTM


## Int 18h
- Since PnP systems (1996): return to Bootstrap Loader (Int 19h), boot next device
	- Call if failed to boot. Alternative: RETF/LRET on the original stack passed by BIOS.
	- http://www.scs.stanford.edu/05au-cs240c/lab/specsbbs101.pdf
 		- BIOS Boot Specification - Version 1.01 - January 11, 1996 - Stanford Secure
- Before PnP: Diskless boot hook (start cassette basic)
	- http://www.ctyme.com/intr/rb-2241.htm

## Int 19h
- Bootstrap Loader executed at end of POST
- http://stanislavs.org/helppc/int_19.html


