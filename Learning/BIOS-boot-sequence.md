---
title: BIOS boot sequence
---

Steps from BIOS bootup to Bootmanager menu. A digest of the scattered information available about the bootup process, services provided by BIOS, and common practices.
For details about actual bootmanagers' implementation take a look at [Bootmanager stages](Learning/Bootmanager-stages).

##### Contents
<!-- MarkdownTOC levels="1,2" autolink="true" -->

- [BIOS to Stage 1](#bios-to-stage-1)
	- [MBR \(Master Boot Record\)](#mbr-master-boot-record)
	- [PBR \(Partition Boot Record\)](#pbr-partition-boot-record)
	- [Hybrid GPT+MBR](#hybrid-gptmbr)
	- [GPT](#gpt)
- [References](#references)
	- [Hybrid MBR annexes](#hybrid-mbr-annexes)
	- [Articles](#articles)
	- [Wiki](#wiki)
	- [Dissecting bootsectors](#dissecting-bootsectors)
	- [Forum](#forum)

<!-- /MarkdownTOC -->



# BIOS to Stage 1

## MBR (Master Boot Record)
- 440 byte - bootcode
- 4 bytes Disk UID (introduced by Windows NT, used by Linux as well) + 2 bytes zero
- 4\*16=64 byte - partition table
- 2 byte - (0x55, 0xAA) "Valid bootsector" signature bytes

- http://www.win.tue.nl/~aeb/partitions/partition_types-2.html#ss2.12
	- 2.12 Details for various operating systems - custom fields, MBR interpretations


## PBR (Partition Boot Record)
- aka. OS Boot Record
- confusingly named VBR (Volume Boot Record) by the T13 Technical Committee
- 36 byte - BPB (BIOS Parameter Block), first 3 bytes SHORT JMP to bootcode + NOP, address 14 DWORD boot record sector count (first sector of File Allocation Table)
- 54 / 26 byte - EBPB (Extended Boot Record), Fat32 / Fat12-16


## Hybrid GPT+MBR
- https://www.gnu.org/software/xorriso/man_1_xorriso.html
- https://en.wikipedia.org/wiki/GUID_Partition_Table#Hybrid_MBR_(LBA_0_+_GPT)
  - GPT-aware boot loader. The bootloader in the MBR must not assume a sector size of 512 bytes.[3]
- http://www.rodsbooks.com/gdisk/hybrid.html
The protective EFI GPT (type 0xEE) MBR partition must normally begin on the second sector of the disk, in order to protect the primary GPT data structures.
The second hybridized partition (#3 in GPT) holds a FAT filesystem, I chose to use a type code of 0x0C.
Although 0xEE is a logical choice, using it will cause Mac OS X to interpret the disk as an MBR disk, thus removing the benefits of GPT for that OS.
Third partition: I used 0x0A, which is the code for an OS/2 Boot Manager partition. My reason for using this code is simply that few disk utilities will attempt to do anything with such a partition.
Many other codes will work as well, but all non-0xEE codes will be misleading or ambiguous.


## GPT
https://en.wikipedia.org/wiki/GUID_Partition_Table

- Inner/sub-partitions allowed? Hypothetical:
	- Fix (make unmovable) the bootloader image when stored as a file (core.img).
	- Export *contiguous* ISO image for mounting, without loop device.
	  Makes possible booting LiveCD without loopback.cfg, or parsing syslinux.cfg and patching the kernel command line.



# References

[[Specifications]](Specifications/Specifications)

## Hybrid MBR annexes
- http://www.t13.org/documents/UploadedDocuments/docs2009/e09127r0-EDD-4_Hybrid_MBR_support.pdf
	- ->  OS Type == 0xED  ->  Set to 0xED if the bootable partition is from GPT.
- http://t13.org/Documents/UploadedDocuments/docs2009/e09150r0-EDD-4_Hybrid_MBR_partition_records_annex.pdf
- http://www.t13.org/documents/UploadedDocuments/docs2009/e09127r1-EDD-4_Hybrid_MBR_support.pdf
- http://www.t13.org/documents/UploadedDocuments/docs2009/e09127r2-EDD-4_Hybrid_MBR_boot_code_annex.pdf
- http://www.t13.org/Documents/UploadedDocuments/docs2010/e09127r3-EDD-4_Hybrid_MBR_boot_code_annex.pdf
	- ->  OS Type == guess what would be in the MBR
	- Note: one byte of the GUIDs should have been specified as the mbr partition type
	- %EAX == "!GPT" (0x54504721), %DS:%SI Pointer to the hybrid MBR handover structure
	- %DS:%SI(+0)  - Boot Indicator == 0x80
	- %DS:%SI(+4)  - OS Type == 0xED
	- %DS:%SI(+8)  - Starting LBA == -1, invalid
	- %DS:%SI(+20) - GPT Partition Entry


## Articles
- https://ericmccorkleblog.wordpress.com/2016/05/15/freebsd-efi-bootloader-refactor/
- https://jdebp.eu/FGA/pcat-boot-process.html
- http://www.osdever.net/tutorials/view/loading-sectors
- http://3zanders.co.uk/2017/10/18/writing-a-bootloader3/
- http://bazaar.launchpad.net/~libburnia-team/libisofs/scdbackup/view/head:/doc/boot_sectors.txt#L246


## Wiki
- https://en.wikipedia.org/wiki/Master_boot_record#BIOS_to_MBR_interface
- https://en.wikipedia.org/wiki/Volume_boot_record
- https://en.wikipedia.org/wiki/BIOS_parameter_block
- https://wiki.osdev.org/FAT#BPB_.28BIOS_Parameter_Block.29
- https://www.tldp.org/HOWTO/html_single/Large-Disk-HOWTO/#s4
  - History of BIOS and IDE limits
- https://en.wikipedia.org/wiki/GNU_GRUB


## Dissecting bootsectors
- https://unix.stackexchange.com/questions/259143/how-does-grub-stage1-exactly-access-load-stage-2
- http://thestarman.pcministry.com/asm/mbr/GRUB.htm
- http://thestarman.pcministry.com/asm/mbr/W7MBR.htm
- http://thestarman.pcministry.com/asm/mbr/W7VBR.htm
- http://thestarman.pcministry.com/asm/mbr/W8VBR.htm
- https://github.com/ryran/burg-checkmbr


## Forum
- https://stackoverflow.com/questions/32701854/boot-loader-doesnt-jump-to-kernel-code/32705076#32705076
	- General Tips for Bootloader Development
- https://mjg59.dreamwidth.org/4957.html
	- Further adventures in EFI booting -> how to boot a CD with MBR+GPT+APM on old Macs (Apple PPC) expecting Apple partition map (APM) instead of El Torito
- https://stackoverflow.com/questions/47277702/custom-bootloader-booted-via-usb-drive-produces-incorrect-output-on-some-compute?noredirect=1&lq=1
	- Quirky laptop BIOS patching BPB area on USB flash boot without verifying the code starts with a JMP (EB/E9) to skip the BPB area.

