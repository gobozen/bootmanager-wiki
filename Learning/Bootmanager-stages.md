---
title: Bootmanager stages
---

Steps from BIOS bootup to the boot menu: how it is done by actual bootmanagers?
For details about the facilities available to the early stages take a look at [Boot sequence of the PC](Learning/Boot-sequence-of-the-PC).

##### Contents
<!-- MarkdownTOC levels="1,2" autolink="true" -->

- [Grub2 i386-pc \(bios\) platform](#grub2-i386-pc-bios-platform)
- [Syslinux](#syslinux)
- [CloverEFI on bios](#cloverefi-on-bios)

<!-- /MarkdownTOC -->



## Grub2 i386-pc (bios) platform
Manual:  https://www.gnu.org/software/grub/manual/grub/html_node/Images.html#Images
- [[In pictures]](GRUB/GRUB-boot-sequence-visualized)
- 1\. BIOS loads MBR (1st sector of hdd).
- 2\. MBR loads PBR (1st sector of active partition).
	- Step skipped by Grub in default configuration - when embedded after MBR, before 1st partition.
- 1st stage: Grub's MBR/PBR:  boot.img (stage1 in Grub Legacy)
	- Source: grub-core/boot/i386/pc/boot.S
	- Loaded at 0x7c00.
	- Loads 2nd stage (1 sector, 0x200 bytes) at 0x8000 from absolute sector number (relative to DISK start, not partition).
	- Sector number: 64-bit LBA stored by `grub-mkimage` at offset 0x5c (GRUB_BOOT_MACHINE_KERNEL_SECTOR) / 0x1b0 (for isohybrid).
- 2nd stage: diskboot.img (1st sector of core.img, stage1_5 in Grub Legacy)
	- Source: grub-core/boot/i386/pc/diskboot.S
	- Loaded at 0x8000 absolute = 0x800:0x0 seg:offs (GRUB_BOOT_I386_PC_KERNEL_SEG:0x0)
	- Loads 3rd stage (rest of core.img, stage1_5 in Grub Legacy) at 0x8200 from a blockchain stored in diskboot.img (absolute sector numbers, relative to DISK start, not partition)
- 3rd stage loaded at 0x8200: lzma_decompress.img (core.img 2nd sector)
	- + kernel.img + core modules (filesystems to access /boot/grub - modules and grub.cfg)
	- Source: grub-core/boot/i386/pc/startup_raw.S
	- Saves `boot_drive = %DL`, enters protected mode by calling `real_to_prot`, enables A20 by calling `grub_gate_a20`.
	- Decompresses grub kernel: calls `grub_reed_solomon_recover()`, `_LzmaDecodeA()`
	- Jumps to grub-core/kern/i386/pc/startup.S#`_start` at 0x100000 (GRUB_MEMORY_MACHINE_DECOMPRESSION_ADDR).
	- Contains the rescue prompt, which is run if 4th stage is not found at the configured `$grub_prefix` (stored at the end of the compressed part).
	- Calls `EXT_C(grub_main)` @ grub-core/kern/main.c
- 4th stage: normal.mod
	- Loads grub.cfg
	- Shows menu
Further articles:
- http://moi.vonos.net/linux/Booting_Linux_on_x86_with_Grub2/



## Syslinux
- ldlinux.sys = ldlinux.asm = diskboot.inc (512B) + diskstart.inc (512B) + diskfs.inc
- Standard MBR loads PBR (1st sector of active partition)
- 1st stage: diskboot.inc at 0x7c00 (ldlinux.sys 1st sector)
	- Loads 2nd stage (1 sector) from fixed sector number (64-bit LBA, relative to PARTITION start) stored as immediate value in code at Sect1Ptr0, Sect1Ptr1, with 2 byte gap (0x66 0xba) between the low and high dwords.
- 2nd stage: diskstart.inc at 0x7e00 (ldlinux.sys 2nd sector)
	- Identified by fixed sector number -> not relocatable on disk
	- Begins at ldlinux_sys, entrypoint at ldlinux_ent.
	- Load 3rd stage: rest of ldlinux.sys at load_rest.
	- Sector chain list at SectorPtrs:  96 kb * 2 sector/kb * 1 entry/sector * 10 byte / entry (each entry defines a range of sectors with an 8-byte LBA index + 2-byte sector count).
- 3rd stage: diskfs.inc at 0x8000 (from 3rd sector to end of ldlinux.sys)


## CloverEFI on bios
- BootDuet.S
- Custom MBR adds hybrid GPT support (BootHFS/boot0.s), loads PBR of first active partition in MBR or GPT
- 1st stage: PBR (bootsector) with FAT12/16/32 read implemented (BootHFS/boot1f32.s or boot1x.s for exFat)
- No fixed sector numbers -> boot loader files are relocatable on disk.
- Needs a FAT or HFS+ filesystem, the EFI partition is usually FAT, so it fits well.
- Uses 32-bit partition start LBA passed in DS:SI+8 by MBR.
- Has no knowledge of the 'Hybrid MBR boot code handover structure' containing GPT partition entry at DS:SI+20 and 64-bit LBA in DS:SI+52.
  The 64-bit LBA is truncated to 32-bit and passed in the MBR partition record by boot1f32.s


