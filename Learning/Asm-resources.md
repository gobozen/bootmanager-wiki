---
title: Asm resources
---

##### Contents
<!-- MarkdownTOC levels="1,2" autolink="true" -->

- [The Language](#the-language)
  - [Online assembler, disassembler](#online-assembler-disassembler)
  - [Syntax, dialects: Intel and AT&T](#syntax-dialects-intel-and-att)
  - [Elements of instructions: opcode, operands, registers, addressing, immediate value](#elements-of-instructions-opcode-operands-registers-addressing-immediate-value)
  - [x86 Instruction Set Reference with opcodes](#x86-instruction-set-reference-with-opcodes)
  - [Linker](#linker)
- [System programming](#system-programming)
  - [Protected mode](#protected-mode)
  - [Flat real mode](#flat-real-mode)
  - [Unreal mode](#unreal-mode)
  - [A20 misery](#a20-misery)
  - [Memory management on the OS level](#memory-management-on-the-os-level)
  - [Memory allocators - the Slab](#memory-allocators---the-slab)
- [Tools: compilers, disassemblers](#tools-compilers-disassemblers)
  - [Triton](#triton)
  - [Keystone-Capstone-Unicorn](#keystone-capstone-unicorn)

<!-- /MarkdownTOC -->



# The Language


## Online assembler, disassembler

- http://shell-storm.org/online/Online-Assembler-and-Disassembler/
  - has 16-bit x86 and many others: wrappers around the Keystone and Capstone projects.
- https://defuse.ca/online-x86-assembler.htm
  - 32/64-bit: x86/x64


## Syntax, dialects: Intel and AT&T

- https://stackoverflow.com/questions/15017659/how-to-read-the-intel-opcode-notation/41616657#41616657
  - 3.1.1.1 Opcode Column in the Instruction Summary Table (Instructions without VEX Prefix)
- Intel and AT&T Syntax
  - https://imada.sdu.dk/Courses/DM18/Litteratur/IntelnATT.htm
- AT&T Syntax versus Intel Syntax
  - http://linux.web.cern.ch/linux/scientific4/docs/rhel-as-en-4/i386-syntax.html
  - https://sourceware.org/binutils/docs/as/i386_002dVariations.html#i386_002dVariations
- http://www.ibiblio.org/gferg/ldp/GCC-Inline-Assembly-HOWTO.html#s3


## Elements of instructions: opcode, operands, registers, addressing, immediate value

- http://www.cs.virginia.edu/~evans/cs216/guides/x86.html
  - x86 Assembly Guide
- http://www.c-jump.com/CIS77/ASM/Addressing/
  - Instruction Operand Addressing on x86


## x86 Instruction Set Reference with opcodes

- https://c9x.me/x86/
  - Mirror: https://x86.puri.sm/
- http://faydoc.tripod.com/cpu/index.htm
  - Intel Pentium Instruction Set Reference
- https://www.felixcloutier.com/x86/index.html
  - x86 and amd64 instruction reference
- http://www.fermimn.gov.it/linux/quarta/x86/index.htm
  - 80386 Instruction Set
- http://ref.x86asm.net/index.html
  - X86 Opcode and Instruction Reference
- https://en.wikipedia.org/wiki/X86_instruction_listings
  - x86 instruction listings


## Linker

- http://www.scoberlin.de/content/media/http/informatik/gcc_docs/ld_3.html




# System programming


## Protected mode

- https://www.codeproject.com/Articles/45788/The-Real-Protected-Long-mode-assembly-tutorial-for
- http://viralpatel.net/taj/tutorial/protectedmode.php


## Flat real mode


## Unreal mode

- Tomasz Grysztar (Flat assembler): https://board.flatassembler.net/topic.php?t=11940
- http://www.os2museum.com/wp/a-brief-history-of-unreal-mode/


## A20 misery

- https://www.win.tue.nl/~aeb/linux/kbd/A20.html


## Memory management on the OS level

- x86 Paging Tutorial
  - http://www.cirosantilli.com/x86-paging/
- Difference Between Contiguous and Noncontiguous Memory Allocation
  - https://techdifferences.com/difference-between-contiguous-and-non-contiguous-memory-allocation.html
- Difference Between Internal and External fragmentation
  - https://techdifferences.com/difference-between-internal-and-external-fragmentation.html
- Memory management
  - https://en.wikipedia.org/wiki/Memory_management


## Memory allocators - the Slab
- The SLAB Memory Allocator
  - http://3zanders.co.uk/2018/02/24/the-slab-allocator/
  - https://en.wikipedia.org/wiki/Slab_allocation




# Tools: compilers, disassemblers


## Triton
- https://triton.quarkslab.com/
Triton is a dynamic binary analysis (DBA) framework. It provides internal components like a Dynamic Symbolic Execution (DSE) engine, a Taint Engine, AST representations of the x86 and the x86-64 instructions set semantics, SMT simplification passes, an SMT Solver Interface and, the last but not least, Python bindings. Based on these components, you are able to build program analysis tools, automate reverse engineering and perform software verification.


## Keystone-Capstone-Unicorn
Keystone assembler framework: Core (Arm, Arm64, Hexagon, Mips, PowerPC, Sparc, SystemZ & X86) + bindings
- https://github.com/keystone-engine/keystone
- http://www.keystone-engine.org

Capstone disassembly/disassembler framework: Core (Arm, Arm64, M68K, Mips, PPC, Sparc, SystemZ, X86, X86_64, XCore) + bindings (Python, Java, Ocaml, PowerShell)
- https://github.com/aquynh/capstone
- http://www.capstone-engine.org

Unicorn CPU emulator framework (ARM, AArch64, M68K, Mips, Sparc, X86)
- https://github.com/unicorn-engine/unicorn
- http://www.unicorn-engine.org


