---
title: Embedded scripting languages
---

Lua is the most known lightweight embedded scripting language with a history since 1993. It is an option to include a Lua engine in grub. In fact, the grub-extras repository has it integrated, and was used for boot-time autodiscovery of Iso images, Zfs boot entries, but it's not part of the main Grub source.
There are also many recent scripting solutions to consider. This document explores features of these script engines. Features I look for would enable the distributors and users to implement advanced boot-time functionality such as:
- automated discovery of non-standard configurations
- dynamic generation/modification of kernel parameters
- user interfaces.

##### Contents
<!-- MarkdownTOC levels="1,2" autolink="true" -->

- [Modern alternative to Lua](#modern-alternative-to-lua)
	- [Wren](#wren)
- [Lua wrappers](#lua-wrappers)
	- [hLua](#hlua)
	- [rLua](#rlua)
- [Lua VMs](#lua-vms)
	- [Go-Lua](#go-lua)
	- [Fengari](#fengari)
- [Running on Lua](#running-on-lua)
	- [MoonScript](#moonscript)
	- [Shine](#shine)
- [Reactive Lua](#reactive-lua)
	- [RxLua](#rxlua)
	- [FRLua](#frlua)
	- [Lua Fun](#lua-fun)
	- [Lua coroutines](#lua-coroutines)
	- [A Reactive Game Stack](#a-reactive-game-stack)
- [Gui toolkit with Lua](#gui-toolkit-with-lua)
	- [TekUI](#tekui)
- [Script languages implemented in Rust](#script-languages-implemented-in-rust)
	- [Collections](#collections)
	- [Moss](#moss)
	- [Gluon](#gluon)
	- [Dyon](#dyon)
	- [Rhai](#rhai)
	- [Monkey](#monkey)
- [Python](#python)

<!-- /MarkdownTOC -->




# Modern alternative to Lua

## Wren
- https://github.com/wren-lang/wren



# Lua wrappers


## hLua
This library is a high-level binding for Lua 5.2. You don't have access to the Lua stack, all you can do is read/write variables (including callbacks) and execute Lua code.
- https://github.com/tomaka/hlua


## rLua
High level Lua bindings to Rust.
rlua is NOT designed to be a perfect zero cost wrapper over the Lua C API, because such a wrapper cannot maintain the safety guarantees that rlua is designed to have.
- https://github.com/chucklefish/rlua



# Lua VMs


## Go-Lua
A Lua VM in pure Go.
- https://github.com/Shopify/go-lua


## Fengari
Fengari (Moon in greek) is the Lua VM written in JavaScript. It uses JavaScript's garbage collector so that interoperability with the DOM is non-leaky.
- https://fengari.io/
- fengari-web.js  ->  13 kloc



# Running on Lua


## MoonScript
The CoffeeScript of Lua, whitespace sensitive. A dynamic scripting language that compiles into Lua.
- Mature, compiler runs in Lua.
- http://moonscript.org/
- Compatible with the tooling available for Lua, such as alternative Jit implementations.


## Shine
A Shiny Lua Dialect.
- Compiler runs in Lua.
- https://github.com/richardhundt/shine#philosophy




# Reactive Lua


## RxLua
Reactive Extensions for Lua.  RxLua gives Lua the power of Observables, which are data structures that represent a stream of values that arrive over time. They're very handy when dealing with events, streams of data, asynchronous requests, and concurrency.
- https://github.com/bjornbytes/RxLua


## FRLua
FRLua is a library inspired by Bacon.js to provide Functional Reactive programming capabilities in Lua. It is targeted at luajit 2.1 and lua >=5.1 <5.4. This is version 0.1.3 of the library. This package uses semver. It is currently implemented in pure lua.
- https://github.com/aiverson/frlua


## Lua Fun
Lua Fun is a high-performance functional programming library designed for LuaJIT tracing just-in-time compiler.
The library provides a set of more than 50 programming primitives typically found in languages like Standard ML, Haskell, Erlang, JavaScript, Python and even Lisp. High-order functions such as map(), filter(), reduce(), zip() will help you to write simple and efficient functional code.
- https://luafun.github.io/intro.html


## Lua coroutines
Using Lua's coroutines in your game
- http://www.jonathanfischer.net/lua-coroutines/


## A Reactive Game Stack
A Reactive Game Stack: Using Erlang, Lua and VoltDB to Enable a Non-Sharded Game World
- https://conferences.oreilly.com/oscon/oscon2014/public/schedule/detail/35038
- http://anasta.net/video/news/a-reactive-game-stack-using-erlang-lua-and-voltdb-to-enable-a-nonsharded-game-world.html




# Gui toolkit with Lua

## TekUI
TekUI is a small, freestanding and portable graphical user interface (GUI) toolkit written in Lua and C. It was initially developed for the X Window System and has been ported to Linux framebuffer, raw framebuffer, DirectFB, Windows, and other displays since.
- http://tekui.neoscientists.org/index.html
- http://tekui.neoscientists.org/overview.html#embedded  -  Fitness for the use in embedded systems



# Script languages implemented in Rust


## Collections
https://www.slant.co/improve/topics/7811/~scripting-languages-for-use-with-rust
- What are the best scripting languages for use with Rust?
https://github.com/ruse-lang/langs-in-rust
- Langs In Rust
https://github.com/rust-unofficial/awesome-rust#scripting
- Scripting at awesome-rust



## Moss
Experimental Moss interpreter in Rust
- https://johnbsmith.github.io/moss/doc/moss/toc.htm
- Non-pure functional, academic style.



## Gluon
- https://github.com/gluon-lang/gluon
A static, type inferred and embeddable language written in Rust. http://gluon-lang.org/try
Gluon is a small, statically-typed, functional programming language designed for application embedding.

### Features
* **Statically typed** - Static typing makes it easier to write safe and efficient interfaces between gluon and the host application.
* **Type inference** - Type inference ensures that types rarely have to be written explicitly giving all the benefits of static types with none of the typing.
* **Simple embedding** - Marshalling values to and from gluon requires next to no boilerplate, allowing functions defined in [Rust][] to be [directly passed to gluon][easy_embed].
* **UTF-8 by default** - Gluon supports unicode out of the box with utf-8 encoded strings and unicode codepoints as characters.
* **Separate heaps** - Gluon is a garbage-collected language but uses a separate heap for each executing gluon thread. This keeps each heap small, reducing the overhead of the garbage collector.
* **Thread safe** - Gluon is written in Rust, which guarantees thread safety. Gluon keeps the same guarantees, allowing multiple gluon programs to run in parallel ([example][parallel])\*
[easy_embed]:http://gluon-lang.org/book/embedding-api.html
[parallel]:https://github.com/gluon-lang/gluon/blob/master/tests/parallel.rs

### Goals
These goals may change or be refined over time as I experiment with what is possible with the language.
* **Embeddable** - Similiar to [Lua][Lua] - it is meant to be included in another program which may use the virtual machine to extend its own functionality.
* **Statically typed** - The language uses a [Hindley-Milner based type system][hm] with some extensions, allowing simple and general type inference.
* **Tiny** - By being tiny, the language is easy to learn and has a small implementation footprint.
* **Strict** - Strict languages are usually easier to reason about, especially considering that it is what most people are accustomed to. For cases where laziness is desired, an explict type is provided.
* **Modular** - The library is split into parser, typechecker, and virtual machine + compiler. Each of these components can be use independently of each other, allowing applications to pick and choose exactly what they need.
[hm]:https://en.wikipedia.org/wiki/Hindley%E2%80%93Milner_type_system
[prelude]:https://github.com/gluon-lang/gluon/blob/master/std/prelude.glu

### Inspiration
This language takes its primary inspiration from [Lua][Lua], [Haskell][Haskell] and [OCaml][OCaml].
[Lua]: http://www.lua.org
[Haskell]: http://www.haskell.org
[OCaml]: http://www.ocaml.org
[Rust]: http://www.rust-lang.org



## Dyon
A rusty dynamically typed scripting language
- https://github.com/PistonDevelopers/dyon
- https://github.com/PistonDevelopers/dyon-tutorial/tree/master/src
- [Tutorial](http://www.piston.rs/dyon-tutorial/)
- [Dyon Snippets](https://github.com/PistonDevelopers/dyon_snippets)
- [/r/dyon](https://www.reddit.com/r/dyon/)

### Motivation and goals
- During the first week of coding, a way to do lifetime checking on function arguments was discovered
- A different approach to code organization was explored by adding the ability to dynamically load modules
- For nice error handling, added option, result and `?` operator
- Mutability check to improve readability
- Mathematical loops and unicode symbols to improve readability
- Go-like coroutines to add multi-thread support
- Ad-hoc types for extra type safety

### Dyon is different!
If you have used another programming language before,
there are some things worth to keep in mind:

Dyon has a limited memory model because of the lack of a garbage collector.
The language is designed to work around this limitation.

The language takes ideas from Javascript, Go and Rust, but focuses on practical features for making games:

- Optional type system with ad-hoc types
- Similar object model to Javascript, but without `null`
- Built-in support for 4D vectors, HTML hex colors
- Go-like coroutines

There are plenty of new ideas in Dyon:

- Lifetime check for function arguments
- Use `return` as local variable
- Mathematical loops
- Current objects
- Secrets
- Grab expressions
- Dynamic modules as a way of organizing code



## Rhai
Rhai - An embedded scripting language for Rust
- https://github.com/jonathandturner/rhai
- Syntax imitates Rust

Rhai's current feature set:
- Easy integration with Rust functions and data types
- Fairly efficient (1 mil iterations in 0.75 sec on my 5 year old laptop)
- Low compile-time overhead (~0.6 sec debug/~3 sec release for script runner app)
- Easy-to-use language based on JS+Rust
- Support for overloaded functions
- No additional dependencies
- No unsafe code



## Monkey
- Rust implementation:  https://github.com/Rydgel/monkey-rust
- Explained in the book ["Writing An Interpreter In Go"](https://interpreterbook.com/#the-monkey-programming-language).
- Syntax similar to JS:  `function` -> `fn`




# Python
The [Intel BIOS Implementation Test Suite (BITS)](https://biosbits.org/) is an implementation of Python for GRUB and EFI without an underlying operating system. Used to run Bios and Cpu tests.
- https://lwn.net/Articles/692638/
- https://news.ycombinator.com/item?id=12084122
- https://www.youtube.com/watch?v=bYQ_lq5dcvM




