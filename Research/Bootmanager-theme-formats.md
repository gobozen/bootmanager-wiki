---
title: Bootmanager theme formats
---

Summary of theme formats in open-source bootmanagers.

##### Contents
<!-- MarkdownTOC levels="1,2" autolink="true" -->

- [Grub](#grub)
- [Burg](#burg)
- [rEFInd](#refind)
- [CloverEFI](#cloverefi)
- [SysLinux gfxboot](#syslinux-gfxboot)
- [Plymouth - after boot, until login](#plymouth---after-boot-until-login)
- [Kernel based bootsplash](#kernel-based-bootsplash)

<!-- /MarkdownTOC -->



## Grub
- http://wiki.rosalab.ru/en/index.php/Grub2_theme_tutorial
- https://www.gnu.org/software/grub/manual/grub/html_node/Theme-file-format.html
- Syntax of `theme.txt`: curly braces delimited tree structure `+ boot_menu { ... }`
- Elements: `boot_menu, canvas, hbox, vbox, label, image, progressbar, circular_progress`



## Burg
- Format and install: https://help.ubuntu.com/community/Burg
- Module: coreui
- Syntax of `theme` similar to Grub: `screen { ... panel { ... } }`
- Elements: `panel, screen, text, image, edit, term, password, progressbar, circular_progress, class, menu, onkey, mapkey`



## rEFInd
- http://www.rodsbooks.com/refind/themes.html

#### Images
- `background.png, selection_{big,small}.png, screenshot.png`
- icons/ `func_*.png, os_*.png, arrow{_left,_right}.png, normal_recovery.png`

#### Theme - flat config
- theme.conf - `hideui singleuser,hints,arrows,label,badges,   icons_dir .../icons, banner .../background.png, banner_scale fillscreen, selection_{big,small} .../*,   showtools reboot,shutdown`
- https://github.com/EvanPurkhiser/rEFInd-minimal/blob/master/theme.conf
- https://github.com/JohnTrentonCary/rEFInd-Metro/blob/master/theme.conf
- https://github.com/lukechilds/refind-ambience/blob/master/theme.conf



## CloverEFI
- https://clover-wiki.zetam.org/Design
- https://clover-wiki.zetam.org/Theme-database
- https://sourceforge.net/p/cloverefiboot/themes/ci/master/tree/themes/

#### Images
- `banner.png, background.png, Selection_{big,small}.png`
- icons/ `func_*.png, os_*.icns, tool_*.icns, vol_*.icns, apple.png, pointer.icns, pointer.png`
- scrollbar/ `bar_{start,fill,end}.png, scroll_{start,fill,end}.png, {up,down}_button.png`

#### Theme - plist
- theme.plist - `{ Author, Description, Year="2014",  Theme { Background { Path="background.png", Type="Scale" },  Badges { Inline=false, Show=true , Swap=true },  Banner="banner.png",  Font { CharWidth=10, Path="font.png", Type="Load" },  Selection { Big="Selection_big.png", Small="Selection_small.png", Color="0x00000065" }  } }`



## SysLinux gfxboot
- compiled binary



## Plymouth - after boot, until login
- https://www.freedesktop.org/wiki/Software/Plymouth/Scripts/
- http://blog.fpmurphy.com/2009/09/project-plymouth.html
- https://wiki.ubuntu.com/BootGraphicsArchitecture
  - Kernel video initialization, Plymouth

- http://brej.org/blog/?cat=16
  - Archive for the ‘ Plymouth ’ Category
- http://brej.org/blog/?p=158
  - 2009/12/04 Plymouth theming guide (part 1)
- http://brej.org/blog/?p=174
  - 2009/12/06 Plymouth theming guide (part 2)
- http://brej.org/blog/?p=197
  - 2009/12/07 Plymouth theming guide (part 3)
- http://brej.org/blog/?p=238
  - 2009/12/16 Plymouth theming guide (part 4)



## Kernel based bootsplash
- 2018-Jan: Only in Manjaro, not mainline.
- https://patchwork.freedesktop.org/series/35310/
- https://lists.freedesktop.org/archives/dri-devel/2018-January/162869.html
- https://patchwork.freedesktop.org/series/35872/
- https://lists.freedesktop.org/archives/dri-devel/2018-February/166922.html
- https://forum.manjaro.org/t/bootsplash-provided-by-the-kernel/34467
- raw (RGB) images + positioning + animgif-like animation



