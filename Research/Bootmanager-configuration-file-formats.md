---
title: Bootmanager configuration file formats
---

This page is a collection of file formats suitable for configuration, and comparison of their features.
The purpose is to evaluate their usability and benefits for bootloaders, primarily to simplify Grub2's conf files.

##### Contents
<!-- MarkdownTOC levels="1,2" autolink="true" -->

- [Evaluation](#evaluation)
  - [What qualities I've been looking for](#what-qualities-ive-been-looking-for)
  - [Tl;Dr-Personal opinion](#tldr-personal-opinion)
  - [Features I find beneficial in this application](#features-i-find-beneficial-in-this-application)
- [Established configuration formats](#established-configuration-formats)
  - [Toml](#toml)
  - [Yaml](#yaml)
  - [Hjson](#hjson)
  - [Hocon](#hocon)
- [Json variants](#json-variants)
  - [Curated lists of Json alternatives](#curated-lists-of-json-alternatives)
  - [Uson](#uson)
  - [JsonX](#jsonx)
  - [HanSON](#hanson)
  - [Json5](#json5)
  - [Ucl](#ucl)
  - [Hcl](#hcl)
  - [Jsof](#jsof)
  - [Cson](#cson)
  - [Axon](#axon)
  - [Mson](#mson)
  - [SON](#son)
- [Examples](#examples)
  - [Toml](#toml-1)
  - [Yaml](#yaml-1)
  - [Hocon](#hocon-1)
  - [Mson](#mson-1)
- [Examples - submenu indexed by title, not id](#examples---submenu-indexed-by-title-not-id)
  - [Toml](#toml-2)
  - [Yaml](#yaml-2)
  - [Hocon](#hocon-2)
- [Examples - current bootmanagers](#examples---current-bootmanagers)
  - [rEFInd](#refind)
  - [Grub2](#grub2)
  - [Grub Legacy](#grub-legacy)
  - [SysLinux](#syslinux)

<!-- /MarkdownTOC -->



# Evaluation

## What qualities I've been looking for
- The document structure should be easily recognizable without prior knowledge. Depends on the position and choice (or absence) of separators: braces, indentation.
- The syntax should be easy to read. This boils down to the choice of separators, for ex. `:` is subtler than `=`, more streamlined, while `=` has more emphasis, separates 2 strings more explicitly.
- Have minimal special cases (principle of least surprise) to make it less error-prone and the parser simple.
- Have minimal syntax overhead (noise): the easier to type the less mistakes made.
- Be friendly to version-control: when a change is made, modify only the lines where the data changed. Extra `,` should not make it necessary to commit a line where the data did not change.
- There are many awesome features of the enlisted formats that aren't important for bootloaders, therefore I don't mention them.


## Tl;Dr-Personal opinion

The ini-style of [Toml](#toml) is well-suited for boot entries, but the unquoted strings feature is missing. [[Example 1]](#toml-1)  [[Example 2]](#toml-2).

Unqouted file paths and kernel parameters is the norm in configuration of open-source bootmanagers. Most people would miss those quotes at least once, resulting in major headache to make the system bootable again, therefore using a modified syntax and parser is necessary.

[Yaml](#yaml) replaces curly braces with indentation (aka. significant whitespace). [[Example 1]](#yaml-1) [[Example 2]](#yaml-2). Carefully formatted SysLinux [[Example]](#syslinux) and Grub Legacy [[Example]](#grub-legacy) config files looked like that. Unquoted keys, values and the lack of braces makes the representation of small objects very readable.

A minimal subset of Yaml is usable and appropriate for a config file: objects, key-value pairs, unquoted, single-, double-quoted strings. All other features would be dead code in the often space-limited bootmanagers, and can be ignored. Perhaps this necessitates writing a custom parser instead of using one that is available and tested.

[Hocon](#hocon) is very expressive, cross-references being an exclusive feature that can be used to refactor previously duplicated properties. [[Example 1]](#hocon-1) [[Example 2]](#hocon-2). Is curly-brace syntax comfortable to a user without any knowledge of languages in the C syntax family?


## Features I find beneficial in this application
- Unquoted keys and values: a must for readability. Space, dot, unescaped backslash all allowed until the key-value separator or the newline.
- Doublequote `""` for strings that parse escape sequences as in JS: `\\`, `\"`, etc.
- Singlequote `''` for unescaped strings, `'you''re'` is parsed as `"you're"`.
- Interchangeable `:`, `=`
- Optional commas at end of line in objects and arrays. Optional trailing comma (for version-control).
- Primitive value tokens: `null`, `false`, `true`. Add one more character to it and it becomes an unquoted string.
- Keys are split on dots to form a path: `foo.bar` === `"foo"."bar"` === `foo["bar"]`
- Small objects without curly braces, delimited by the change of indentation.  Arrays always in square brackets.
- Objects with an `[object.path]` prolog to emphasise self-standing subtrees or high-level partitioning of the configuration tree. No necessary indentation, no curly braces. Closing delimiter is the next line that starts with `[`.
- `[]` should close the last object and reset to the root object.
- Object merging, inclusion allows separation of upstream, distribution config and user settings.




# Established configuration formats


## Toml
"Tom's Obvious, Minimal Language"
- https://github.com/toml-lang/toml
- 21st century ini file syntax with well-defined semantics able to represent an object tree.
- Json superset: can parse json files.
- Used in:
    [Netlify deploy context](https://www.netlify.com/docs/netlify-toml-reference/)
    [Rust package manager: Cargo](https://doc.rust-lang.org/stable/cargo/reference/manifest.html)
    [Go dependency manager: Dep](https://golang.github.io/dep/docs/Gopkg.toml.html)
    [Python dependency manager: Poetry](https://github.com/sdispater/poetry#introduction)


## Yaml
- http://yaml.org/
- Inventive format, used industry-wide for configuration and data.
- Complex, far-reaching feature-set. Compact syntax results in many special cases in parser, arrays are easy to confuse.
- Json superset: can parse json files.
- Used in: static website generators.



## Hjson
- https://hjson.org/

#### Common:
- unquoted keys if they are JavaScript identifier
- unquoted strings: no `\` escape necessary
- multi-line strings: `''' ... '''`, lines can be indented the same as ''', indentation is eliminated
- optional trailing comma (at end of line and array/object)
- comments: `//`, `#`, `/**/`



## Hocon
"Human-Optimized Config Object Notation"
- https://github.com/lightbend/config/blob/master/HOCON.md

#### Unique features among the variants:
- object merging: define one object in multiple parts. This allows extending objects: a distributor can define an object in its configuration and users can extend, modify it in their own config file.
- substitutions: object (cross-)references with the same syntax as shell-variables (`${foo.bar}`).
- includes inside an object to include a sub-tree (`bar { include "defaults/bar.conf" }`).
- path as key: shorten deep object trees (`foo.bar=`).
- `=` optional before objects (`foo= {}` and `foo: {}` and `foo {}` are the same)
- optional root braces

#### Common:
- unquoted keys, even with whitespace in it (Unique)
- key-value separator: `=` is allowed too besides `:` (only Hocon, JsonX)
- unquoted strings: no `\` escape necessary
- multi-line strings: `""" ... """`, white-space preserved, indentation not possible
- optional trailing comma (at end of line and array/object)
- comments: `//`, `#`. No `/**/`?

#### Used by:
- [Play Web Framework](https://www.playframework.com/documentation/2.5.x/ConfigFile) - pretty good documentation of the format
- [Lightbend - config](https://github.com/lightbend/config#using-hocon-the-json-superset)
- [Puppet](https://puppet.com/blog/managing-hocon-configuration-files-puppet)
- [pyhocon](https://github.com/chimpler/pyhocon) - parser and conversion tool for Python




# Json variants

- Why Json? It is well-known in the industry, arguably the most used text-based serialization format in web development as of 2018.
- Why not Json? It's a machine-to-machine data interchange format, not designed for human editing. It has a strict, simple, homogeneous syntax, benefitting parsers. This comes with some syntactical overweight and quirks, though way less than in Xml.
- Not version-control friendly: comma after last element of array or last field of object is syntax error. When removing the last element one has to remove the coma after the next-to-last, modifying 2 lines instead of one. This doubles commit size and clobbers line history (blame).
- No comments in Json makes it unsuitable for configuration files.
- Double-quotes everywhere are tedious to type, adds noise, makes it hard to read.
- Json variants (or supersets) refine this syntax, are less-known, but easily recognized and adapted.



## Curated lists of Json alternatives

- https://github.com/json-next/awesome-json-next#hjson
  - A Collection of What's Next for Awesome JSON (JavaScript Object Notation) for Structured (Meta) Data in Text - JSON5, HJSON, HanSON, TJSON, SON, CSON, USON, JSONX/JSON11 & Many More

- https://github.com/burningtree/awesome-json#supersets
  - A curated list of awesome JSON libraries and resources - Supersets

- https://www.tbray.org/ongoing/When/201x/2016/08/20/Fixing-JSON
- https://serde.rs/#data-formats



## Uson
"μson (uson) is a shorthand for JSON"
- https://github.com/burningtree/uson

#### Unique:
- unquoted strings
- optional comma between array elements and object fields
- path as key: assignation with colon (`:`) can be repeated to create nested objects
- types: `<type>!<value>`



## JsonX
"JSON with Extensions (JSONX) - also known as JSON: 1.1, JSON v1.1, JSON11, JSON XI, or JSON II"
- https://json-next.github.io/#whats-different
- includes all JSON extensions from HanSON and SON

#### Unique:
- array of objects: [{ fields1 },{ fields2 }]  =>  < fields1 - fields2 >

#### Common:
- unquoted keys if they are JavaScript identifier
- key-value separator: `=` is allowed too besides `:` (only Hocon, JsonX)
- unquoted strings: only JavaScript identifier allowed (no whitespace)
- multi-line strings: single back-ticks as quotes (\`\`), white-space preserved, indentation not possible
- optional trailing comma (at end of line and array/object)
- comments: `//`, `#`, `/**/`



## HanSON
"JSON for Humans - with unquoted identifiers, multi-line strings and comments"
- https://github.com/timjansen/hanson

#### Common:
- unquoted keys if they are JavaScript identifier
- multi-line strings: single back-ticks as quotes (\`\`), white-space preserved, indentation not possible
- optional trailing comma (at end of line and array/object)
- comments: `//`, `/**/`. No `#`.

#### Unique:
- single-quoted strings `'text'`



## Json5
"JSON for Humans"
- https://json5.org/
- https://spec.json5.org/

#### Common:
- unquoted keys if they are JavaScript identifier
- comments: `//`, `/**/`. No `#`.

#### Unique:
- single-quoted strings `'text'`
- escaping (`\`) new line characters in strings
- character escapes in strings



## Ucl
"Universal configuration library parser"
- https://github.com/vstakhov/libucl
- similar to theme format


## Hcl
HCL is the "HashiCorp configuration language".
- https://github.com/hashicorp/hcl
- like, but simpler than Ucl


## Jsof
liberal JSON
- https://github.com/drom/jsof


## Cson
"CoffeeScript-Object-Notation". Same as JSON but for CoffeeScript objects.
- https://github.com/bevry/cson
- like a simple Yaml, objects don't have curly braces, but arrays have `[]`


## Axon
AXON is "eXtended Object Notation"
- https://intellimath.bitbucket.io/axon
- formatted form in both JSON/C and YAML/Python style

#### Unique:
- one-liner lists with whitespace separator: `[1 3.14 3.25D ∞ -∞ ?]`
- distinguished ordered list/dict `[]`, unordered set/dict/node `{}`, and tuple `()`


## Mson
"Markdown Syntax for Object Notation"
- https://github.com/apiaryio/mson

#### Unique:
- markdown-style list of properties or array elements


## SON
- https://github.com/aleksandergurin/simple-object-notation

#### Common:
- optional trailing comma (at end of line and array/object)
- comments: Only `#`.




# Examples


## Toml
- Validator: https://www.tomllint.com/
```toml
title= 'Gnu+Linux'

root= { label= 'GnuLinux1', hint=  '(hd0,1)', uuid=  '12345678-9abc-def0-1234-56789abcdef0' }
# Redundant identification: if the uuid is mistyped the label will identify it and boot will succeed

linux=        '/boot/vmlinuz-linux'
initrd=       '/boot/initramfs-linux.img'
initrd_early= '/boot/intel-ucode.img'
options=      'ro root  quiet splash=silent vt.handoff=7'
# Grub includes the root option that identified the volume: 'root=UUID=...' or 'root=LABEL=GnuLinux1'


[submenu.normal]
title=    'Latest kernel'

[submenu.lts]
title=    'Long-term-service kernel'
linux=    '/boot/vmlinuz-linux-lts'
initrd=   '/boot/initramfs-linux-lts.img'

[submenu.fallback]
title=        'Failed to boot? Try this fallback.'
savedefault=  false    # don't make this default
gfxpayload=   'text'   # show kernel messages
initrd_early= ''
linux=        '/boot/vmlinuz-linux-lts'
initrd=       '/boot/initramfs-linux-lts-fallback.img'  # initrd with all the unused drivers
options=      'ro root'

[submenu.rootconsole]
title=        'Service console'
savedefault=  false
gfxpayload=   'text'
initrd_early= ''
linux=        '/boot/vmlinuz-linux-lts'
initrd=       '/boot/initramfs-linux-lts-fallback.img'
options=      'ro root  single'
```


## Yaml
- Validator: https://codebeautify.org/yaml-validator
```yaml
title: Gnu+Linux

root:
  label: GnuLinux1
  hint:  (hd0,1)
  uuid:  12345678-9abc-def0-1234-56789abcdef0

linux:        /boot/vmlinuz-linux
initrd:       /boot/initramfs-linux.img
initrd_early: /boot/intel-ucode.img
options:      ro root  quiet splash=silent vt.handoff=7

submenu:
  normal:
    title:    Latest kernel
  lts:
    title:    Long-term-service kernel
    linux:    /boot/vmlinuz-linux-lts
    initrd:   /boot/initramfs-linux-lts.img
  fallback:
    title:        Failed to boot? Try this fallback.
    savedefault:  false
    gfxpayload:   text
    linux:        /boot/vmlinuz-linux-lts
    initrd:       /boot/initramfs-linux-lts-fallback.img
    initrd_early: ''
    options:      ro root
  rootconsole:
    title:        Service console
    savedefault:  false
    gfxpayload:   text
    linux:        /boot/vmlinuz-linux-lts
    initrd:       /boot/initramfs-linux-lts-fallback.img
    initrd_early: ''
    options:      ro root  single
```


## Hocon
- Validator: http://www.hoconlint.com/
[Unquoted strings](https://github.com/lightbend/config/blob/master/HOCON.md#unquoted-strings):
- it does not contain "forbidden characters": '$', '"', '{', '}', '[', ']', ':', '=', ',', '+', '#', backtick, '^', '?', '!', '@', '*', '&', '\' (backslash), or whitespace.
- it does not contain the two-character string "//" (which starts a comment)
- its initial characters do not parse as true, false, null, or a number.
```hocon
title: "Gnu+Linux"

root {
  label: GnuLinux1
  hint:  "(hd0,1)"
  uuid:  12345678-9abc-def0-1234-56789abcdef0
}

root_option:        "ro root=UUID=${root.uuid}"
bootsplash_option:  "quiet splash=silent vt.handoff=7"

linux:        /boot/vmlinuz-linux
initrd:       /boot/initramfs-linux.img
initrd_early: /boot/intel-ucode.img
options:      ${root_option} ${bootsplash_option}


submenu {
  normal {
    title:    Latest kernel
  }
  lts {
    title:    Long-term-service kernel
    linux:    /boot/vmlinuz-linux-lts
    initrd:   /boot/initramfs-linux-lts.img
  }
  fallback {
    title:        "Failed to boot? Try this fallback."
    savedefault:  false  // don't make this default
    gfxpayload:   text    // show kernel messages
    initrd_early: null
    linux:        /boot/vmlinuz-linux-lts
    initrd:       /boot/initramfs-linux-lts-fallback.img  // initrd with all the unused drivers
    options:      ${root_option}
  }
  rootconsole {
    title:        Service console
    savedefault:  false
    gfxpayload:   text
    initrd_early: null
    linux:        /boot/vmlinuz-linux-lts
    initrd:       /boot/initramfs-linux-lts-fallback.img
    options:      ${root_option}  single
  }
}  // submenu
```


## Mson
- Validator:
```mson
- title: "Gnu+Linux"

- root
  - label: GnuLinux1
  - hint:  "(hd0,1)"
  - uuid:  12345678-9abc-def0-1234-56789abcdef0

- root_option:        "ro root=UUID=${root.uuid}"
- bootsplash_option:  "quiet splash=silent vt.handoff=7"

- linux:        /boot/vmlinuz-linux
- initrd:       /boot/initramfs-linux.img
- initrd_early: /boot/intel-ucode.img
- options:      ${root_option} ${bootsplash_option}


- submenu:
  - normal:
    - title:    Latest kernel
  - lts:
    - title:    Long-term-service kernel
    - linux:    /boot/vmlinuz-linux-lts
    - initrd:   /boot/initramfs-linux-lts.img
  - fallback:
    - title:        Failed to boot? Try this fallback.
    - savedefault:  false
    - gfxpayload:   text
    - linux:        /boot/vmlinuz-linux-lts
    - initrd:       /boot/initramfs-linux-lts-fallback.img
    - initrd_early: ''
    - options:      ro root
  - rootconsole:
    - title:        Service console
    - savedefault:  false
    - gfxpayload:   text
    - linux:        /boot/vmlinuz-linux-lts
    - initrd:       /boot/initramfs-linux-lts-fallback.img
    - initrd_early: ''
    - options:      ro root  single
```




# Examples - submenu indexed by title, not id


## Toml
- Validator: https://www.tomllint.com/
```toml
title= 'Gnu+Linux'

root= { label= 'GnuLinux1', hint=  '(hd0,1)', uuid=  '12345678-9abc-def0-1234-56789abcdef0' }
# Redundant identification: if the uuid is mistyped the label will identify it and boot will succeed

linux=        '/boot/vmlinuz-linux'
initrd=       '/boot/initramfs-linux.img'
initrd_early= '/boot/intel-ucode.img'
options=      'ro root  quiet splash=silent vt.handoff=7'
# Grub includes the root option that identified the volume: 'root=UUID=...' or 'root=LABEL=GnuLinux1'


["Latest kernel"]
id=       'normal'

["Long-term-service kernel"]
id=       'lts'
linux=    '/boot/vmlinuz-linux-lts'
initrd=   '/boot/initramfs-linux-lts.img'

["Failed to boot? Try this fallback"]
id=           'fallback'
savedefault=  false    # don't make this default
gfxpayload=   'text'   # show kernel messages
initrd_early= ''
linux=        '/boot/vmlinuz-linux-lts'
initrd=       '/boot/initramfs-linux-lts-fallback.img'  # initrd with all the unused drivers
options=      'ro root'

["Service console"]
id=           'rootconsole'
savedefault=  false
gfxpayload=   'text'
initrd_early= ''
linux=        '/boot/vmlinuz-linux-lts'
initrd=       '/boot/initramfs-linux-lts-fallback.img'
options=      'ro root  single'

```


## Yaml
- Validator: https://codebeautify.org/yaml-validator
```yaml
title: Gnu+Linux

root:
  label: GnuLinux1
  hint: (hd0,1)
  uuid: 12345678-9abc-def0-1234-56789abcdef0

linux:        /boot/vmlinuz-linux
initrd:       /boot/initramfs-linux.img
initrd_early: /boot/intel-ucode.img
options:      ro root  quiet splash=silent vt.handoff=7

submenu:
  Latest kernel:
    id:       normal
  Long-term-service kernel:
    id:       lts
    linux:    /boot/vmlinuz-linux-lts
    initrd:   /boot/initramfs-linux-lts.img
  Failed to boot? Try this fallback.:
    id:           fallback
    savedefault:  false
    gfxpayload:   text
    linux:        /boot/vmlinuz-linux-lts
    initrd:       /boot/initramfs-linux-lts-fallback.img
    initrd_early: ''
    options:      ro root
  Service console:
    id:           rootconsole
    savedefault:  false
    gfxpayload:   text
    linux:        /boot/vmlinuz-linux-lts
    initrd:       /boot/initramfs-linux-lts-fallback.img
    initrd_early: ''
    options:      ro root  single
```


## Hocon
- Validator: http://www.hoconlint.com/
```json
title: "Gnu+Linux"

root {
  label: GnuLinux1
  hint:  "(hd0,1)"
  uuid:  12345678-9abc-def0-1234-56789abcdef0
}

root_option:        "ro root:UUID:${root.uuid}"
bootsplash_option:  "quiet splash:silent vt.handoff:7"

linux:        /boot/vmlinuz-linux
initrd:       /boot/initramfs-linux.img
initrd_early: /boot/intel-ucode.img
options:      ${root_option} ${bootsplash_option}


submenu {
  "Latest kernel" {
    id:       normal
  }
  "Long-term-service kernel" {
    id:       lts
    linux:    /boot/vmlinuz-linux-lts
    initrd:   /boot/initramfs-linux-lts.img
  }
  "Failed to boot? Try this fallback." {
    id:           fallback
    savedefault:  false  // don't make this default
    gfxpayload:   text    // show kernel messages
    linux:        /boot/vmlinuz-linux-lts
    initrd:       /boot/initramfs-linux-lts-fallback.img  // initrd with all the unused drivers
    initrd_early: null
    options:      ${root_option}
  }
  "Service console" {
    id:           rootconsole
    savedefault:  false
    gfxpayload:   text
    linux:        /boot/vmlinuz-linux-lts
    initrd:       /boot/initramfs-linux-lts-fallback.img
    initrd_early: null
    options:      ${root_option}  single
  }
}  // submenu
```




# Examples - current bootmanagers


## rEFInd
- http://www.rodsbooks.com/refind/configfile.html#stanzas

```
menuentry "Gnu+Linux" {

#root  {
#  label  GnuLinux1
#  hint  (hd0,1)
#  uuid  12345678-9abc-def0-1234-56789abcdef0
#}

loader   /boot/vmlinuz-linux
initrd   /boot/intel-ucode.img  /boot/initramfs-linux.img
options  ro root=UUID=12345678-9abc-def0-1234-56789abcdef0  quiet splash=silent vt.handoff=7


submenuentry "Latest kernel" {
}

submenuentry "Long-term-service kernel" {
  loader  /boot/vmlinuz-linux-lts
  initrd  /boot/intel-ucode.img  /boot/initramfs-linux-lts.img
}

submenuentry "Failed to boot? Try this fallback." {
  # savedefault  false  # don't make this default
  graphics off  # show kernel messages
  loader   /boot/vmlinuz-linux-lts
  initrd   /boot/initramfs-linux-lts-fallback.img  # initrd with all the unused drivers
  options  ro root=UUID=12345678-9abc-def0-1234-56789abcdef0
}

submenuentry "Service console" {
  # savedefault  false
  graphics off
  loader   /boot/vmlinuz-linux-lts
  initrd   /boot/initramfs-linux-lts-fallback.img
  options  ro root=UUID=12345678-9abc-def0-1234-56789abcdef0  single
}

} # menuentry
```

- http://www.rodsbooks.com/refind/linux.html#refind_linux
  - refind_linux.conf:  only parameters, not possible to use alternative initrd
  - used for all kernels in same folder

```
"Boot with standard options"  "ro root=UUID=12345678-9abc-def0-1234-56789abcdef0  quiet splash=silent vt.handoff=7"
"Show kernel messages."   "ro root=UUID=12345678-9abc-def0-1234-56789abcdef0"
"Service console: single-user mode"    "ro root=UUID=12345678-9abc-def0-1234-56789abcdef0 showopts single"
```


## Grub2
- http://www.rodsbooks.com/refind/configfile.html#stanzas

```
submenu "Gnu+Linux" {
# skipped duplicated default menuentry before the submenu
# some boilerplate removed:  load_video, gfxpayload, insmod, echo, --hint-...

root = hd0,1
search --no-floppy --set=root --hint-bios=hd0,1  --fs-uuid 12345678-9abc-def0-1234-56789abcdef0
#  --fs-label  "GnuLinux1"


menuentry "Latest kernel" {
  savedefault
  linux    /boot/vmlinuz-linux  ro root=UUID=12345678-9abc-def0-1234-56789abcdef0  quiet splash=silent vt.handoff=7
  initrd   /boot/intel-ucode.img  /boot/initramfs-linux.img
}

menuentry "Long-term-service kernel" {
  savedefault
  linux   /boot/vmlinuz-linux-lts
  initrd  /boot/intel-ucode.img  /boot/initramfs-linux-lts.img
}

menuentry "Failed to boot? Try this fallback." {
  gfxpayload = text  # show kernel messages
  linux    /boot/vmlinuz-linux-lts
  initrd   /boot/initramfs-linux-lts-fallback.img  # initrd with all the unused drivers
  options  ro root=UUID=12345678-9abc-def0-1234-56789abcdef0
}

menuentry "Service console" {
  gfxpayload = text
  linux    /boot/vmlinuz-linux-lts
  initrd   /boot/initramfs-linux-lts-fallback.img
  options  ro root=UUID=12345678-9abc-def0-1234-56789abcdef0  single
}

} # menuentry
```


## Grub Legacy

```
TITLE  --- Gnu+Linux ---

TITLE  Latest kernel
  root    hd0,1
  kernel  /boot/vmlinuz-linux   ro root=UUID=12345678-9abc-def0-1234-56789abcdef0  quiet splash=silent vt.handoff=7
  initrd  /boot/initramfs-linux.img

TITLE  Long-term-service kernel
  root    hd0,1
  kernel  /boot/vmlinuz-linux-lts   ro root=UUID=12345678-9abc-def0-1234-56789abcdef0  quiet splash=silent vt.handoff=7
  initrd  /boot/initramfs-linux-lts.img}

TITLE  Failed to boot? Try this fallback.
  root    hd0,1
  kernel  /boot/vmlinuz-linux-lts
  initrd  /boot/initramfs-linux-lts-fallback.img   ro root=UUID=12345678-9abc-def0-1234-56789abcdef0

TITLE  Service console
  root    hd0,1
  kernel  /boot/vmlinuz-linux-lts
  initrd  /boot/initramfs-linux-lts-fallback.img   ro root=UUID=12345678-9abc-def0-1234-56789abcdef0  single
```


## SysLinux

```
#root  hd0,1
LABEL  --- Gnu+Linux ---

LABEL  Latest kernel
  kernel  /boot/vmlinuz-linux
  append  initrd=/boot/initramfs-linux.img   ro root=UUID=12345678-9abc-def0-1234-56789abcdef0  quiet splash=silent vt.handoff=7

LABEL  Long-term-service kernel
  kernel  /boot/vmlinuz-linux-lts
  append  initrd=/boot/initramfs-linux-lts.img   ro root=UUID=12345678-9abc-def0-1234-56789abcdef0  quiet splash=silent vt.handoff=7

LABEL  Failed to boot? Try this fallback.
  kernel  /boot/vmlinuz-linux-lts
  append  initrd=/boot/initramfs-linux-lts-fallback.img   ro root=UUID=12345678-9abc-def0-1234-56789abcdef0

LABEL  Service console
  kernel  /boot/vmlinuz-linux-lts
  append  initrd=/boot/initramfs-linux-lts-fallback.img   ro root=UUID=12345678-9abc-def0-1234-56789abcdef0  single
```


