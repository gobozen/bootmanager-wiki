---
title: Booting ISO images
---

How to boot one of many ISOs (distibution LiveCDs, rescue disks) stored in a folder, on a HDD or Flash drive?
<br>This page lists ISO multiboot solutions, their benefits and usage scenarios. Updated in April 2018.

##### Contents
<!-- MarkdownTOC levels="1,2" autolink="true" -->

- [Multiple ISO images on one USB drive](#multiple-iso-images-on-one-usb-drive)
	- [multibootusb.org](#multibootusborg)
	- [MultiBoot USB](#multiboot-usb)
	- [GRUB Live ISO Multiboot](#grub-live-iso-multiboot)
	- [Super GRUB2 Disk](#super-grub2-disk)
	- [SystemImageKit](#systemimagekit)
- [Distribution know-how collected by awesome people](#distribution-know-how-collected-by-awesome-people)
- [For developers, experimenters:](#for-developers-experimenters)
	- [Grub2-FileManager, written in Lua](#grub2-filemanager-written-in-lua)
	- [Marcelo Politzer's](#marcelo-politzers)
	- [Jimmy-Z's](#jimmy-zs)
	- [kilbith's](#kilbiths)

<!-- /MarkdownTOC -->



The following projects have different solutions with the same core idea.
To boot from an ISO image one has to know how to pass the path of the ISO file to the kernel (depends on the init scripts in the ISO image initrd), and other kernel parameters.
Friendly distros already included a standard solution in their images: 
- https://www.supergrubdisk.org/wiki/Loopback.cfg
- See also: https://www.gnu.org/software/grub/manual/grub/html_node/Loopback-booting.html

Without `loopback.cfg` the grub script has to know the specific distribution's kernel parameter to pass, and usually parse isolinux.cfg to find out the other kernel parameters.
These projects differ in which distributions they know, and how you can install them. Let's see their pros or cons - depending on your needs.



# Multiple ISO images on one USB drive


## multibootusb.org
http://multibootusb.org
- Graphical installer on Windows _and_ Linux to create bootable USB, suitable for beginners.
- Persistence for distros derived from Ubuntu, Debian, Fedora: http://multibootusb.org/page_guide/#adding-persistence-file
- https://github.com/mbusb/multibootusb


## MultiBoot USB
https://mbusb.aguslr.com/isos.html
- Huge support catalog: 104 distros and recovery tools listed.
- Bash script (Linux only) installs Grub.
- ISOs stored in: `/multibootusb/iso`


## GRUB Live ISO Multiboot
https://github.com/thias/glim
- ~20 distros supported
- Bash script (Linux only) installs Grub
- ISOs stored in: `/boot/iso/*.iso`


## Super GRUB2 Disk
https://www.supergrubdisk.org/wiki/SuperGRUB2Disk
- Robust boot recovery, auto-detecting operating systems
- ISOs stored in: `/boot-isos/` or `/boot/boot-isos/`


## SystemImageKit
https://github.com/probonopd/SystemImageKit
- Steps up the game: Booting ISOs from HDD as resident operating systems.



# Distribution know-how collected by awesome people

- https://github.com/aguslr/multibootusb/tree/master/mbusb.d  -  ~100 configs
- https://github.com/Thermionix/multipass-usb/tree/master/scripts/grub_disk/grub_templates
- http://www.panticz.de/MultiBootUSB
- https://github.com/Jimmy-Z/grub-iso-boot/blob/master/grub.cfg
- https://wiki.archlinux.org/index.php/Multiboot_USB_drive#Boot_entries
- https://help.ubuntu.com/community/Grub2/ISOBoot/Examples




# For developers, experimenters:

## Grub2-FileManager, written in Lua
https://github.com/a1ive/grub2-filemanager
- custom built grub core with lua interpreter


## Marcelo Politzer's
https://github.com/mpolitzer/grub-iso-multiboot
- grub scripts and binaries (custom build) + nice theme
- modified `syslinux_configfile` command to append `$linux_extra` (the $iso_path) to the kernel options
- grub-install, then copy to usb


## Jimmy-Z's
https://github.com/Jimmy-Z/grub-iso-boot


## kilbith's
https://github.com/kilbith/smi


