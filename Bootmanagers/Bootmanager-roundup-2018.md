---
title: Bootmanager roundup 2018
---

This page is a roundup of significant open-source bootmanagers for the PC, in 2018.

##### Contents
<!-- MarkdownTOC levels="1,2" autolink="true" -->

- [User-friendly EFI boot managers](#user-friendly-efi-boot-managers)
	- [rEFInd](#refind)
	- [Clover EFI boot \(works on BIOS systems\)](#clover-efi-boot-works-on-bios-systems)
	- [rEFIt](#refit)
- [Mainstream](#mainstream)
	- [Grub2](#grub2)
	- [Grub2Win](#grub2win)
	- [systemd-boot \(formerly gummiboot\)](#systemd-boot-formerly-gummiboot)
	- [SysLinux](#syslinux)
	- [FreeBSD bootloader](#freebsd-bootloader)
	- [Haiku \(BeOS\) bootloader](#haiku-beos-bootloader)
- [Historical](#historical)
	- [Grub Legacy](#grub-legacy)
	- [Grub4Dos](#grub4dos)
	- [eLiLo](#elilo)
- [Configuration tools](#configuration-tools)
	- [efibootmgr](#efibootmgr)
	- [kernelstub](#kernelstub)
	- [Boot Environments on ZFS filesystems](#boot-environments-on-zfs-filesystems)
- [Purpose-built, specialized bootloaders](#purpose-built-specialized-bootloaders)
	- [alexfru/BootProg](#alexfrubootprog)

<!-- /MarkdownTOC -->



# User-friendly EFI boot managers

- You can boot anything EFI-bootable from the EFI System Partition. These bootmanagers give a nice interface to choose.
- To load your .EFI file or linux kernel from the OS partition, you probably need the driver for the partition's filesystem. Grub's drivers are built for EFI too: https://efi.akeo.ie/



## rEFInd

- Only for UEFI systems. Not a bootloader in principle: depends on UEFI loading the kernel.
- Fork of the abandoned rEFIt.
- Mac-style graphical boot menu, similar, but nicer than UEFI menu designs.
- Out-of-the-box supported filesystems: Fat, Hfs+ on Mac (by UEFI).
- Get more drivers: http://www.rodsbooks.com/refind/drivers.html#finding

#### Unique features
- Boot-time autodetection of bootable Linux, Mac, Windows systems.
- The main menu shows one entry per OS/partition. The fallback/alternative boot configurations are shown in a submenu when you press the Insert, F2, or + key. This keep the boot menu clean.
- Shutdown, reboot, shell, memtest, help, etc. are shown in a separate listing, not mixed with boot entries.
- http://www.rodsbooks.com/refind/using.html

#### Themes
- Some themes available, theme format same as rEFIt with the addition of `theme.conf`.
- Also similar to Clover, most images can be reused as they are, when converting.
- http://www.rodsbooks.com/refind/themes.html

#### Configuration
- Kernels with common names are auto-detected. Specify kernel parameters in `refind_linux.conf` next to the kernel. `refind-install` / `mkrlconf` generates a basic `refind_linux.conf`.
- http://www.rodsbooks.com/refind/linux.html#efistub 4.
- OSes not detected are configured in `refind.conf` with menuentry(s) similar to Grub2's.
- `menuentry <entry title> { loader <kernel image> ; initrd <ramdisk image> ; options <kernel parameters> ; submenuentry { <fallback/alternative config> } }`
- http://www.rodsbooks.com/refind/configfile.html#stanzas

#### References
- https://en.wikipedia.org/wiki/REFInd



## Clover EFI boot (works on BIOS systems)

- rEFIt for BIOS systems: boot hackintosh / windows / linux on BIOS or UEFI system.
- Includes rEFIt: Mac-style graphical boot menu, many themes.
- Emulates a UEFI runtime with the included DUET from TianoCore.org (heavily modified to cater for BIOS vendor peculiarities).
- Out-of-the-box supported filesystems: Fat, Hfs+ on Mac (by UEFI) + ntfs, ext2-4.
- https://clover-wiki.zetam.org/What-is-what#efi-drivers

#### Themes
- https://clover-wiki.zetam.org/Theme-database
- https://sourceforge.net/p/cloverefiboot/themes/ci/master/tree/themes/

#### Configuration
https://clover-wiki.zetam.org/Configuration#Config.plist-structure

#### Documentation
- https://clover-wiki.zetam.org/Contents
- https://sourceforge.net/projects/cloverefiboot/



## rEFIt

- Only for UEFI systems.
- Although abandoned, still in use, somewhat like Grub Legacy.
- http://refit.sourceforge.net/screen.html
- Number of boot entries are limited, list not scrolling.
- Out-of-the-box supported filesystems: Fat, Hfs+ on Mac (by UEFI) + has ext2fs, ReiserFS drivers.



# Mainstream

## Grub2
- The strength of Grub is the wide range of supported platforms, file-systems, operating systems, making it the default choice for distributions and embedded systems.

#### Configuration
- `menuentry <entry title> { linux <kernel image> <kernel parameters>, initrd <ramdisk image> }`


## Grub2Win
- A user-friendly installer and configuration tool for Grub2, running on Windows.
- Grub can be chosen from the Windows boot menu on startup. The installer adds the necessary Boot Configuration Data (BCD) entry for Grub.



## systemd-boot (formerly gummiboot)

- Only for UEFI systems. Tiny. Not a bootloader in principle: depends on UEFI loading the kernel.
- Kernel and initramdisk has to be EFI accessible: on the EFI System Partition, or any FAT filesystem, or using extra EFI filesystem drivers.
- Minimalistic text-mode bootmenu, or single-boot (timeout not set).
- Compared to eLiLo, also supports booting other OS than Linux and shows the available boot entries.
- The UEFI runtime loads the kernel using the EFI stub.

#### Who/when should use it
- Tech-savvy users who don't want graphical bootmenu.
- Who consider Grub, rEFInd overweight for their needs (ex. single-boot scenarios).

#### Configuration
- Few or no distributions support it with maintenance scripts, so you'll have to configure it manually?
- Easy config: has a separate configuration file for each menu entry. Format reminiscent of SysLinux, Grub Legacy.
- `title <entry title> ; linux <kernel image> ; initrd <ramdisk image> ; options <kernel parameters>`
- https://www.freedesktop.org/wiki/Software/systemd/systemd-boot/
- http://www.rodsbooks.com/efi-bootloaders/gummiboot.html
- https://www.freedesktop.org/wiki/Specifications/BootLoaderSpec/
- https://wiki.archlinux.org/index.php/systemd-boot

#### Hotkeys
- 'e' starts a line editor with which you can edit your boot options
- uppercase 'Q' quits from systemd-boot (returns to UEFI boot, which continues with next bootdevice?)
- 'v' shows the systemd-boot and firmware version
- 'F1' summarizes the keys' functions



## SysLinux

#### Who/when should use it
- CD boot, Live CDs (IsoLinux), Network boot (PxeLinux)

#### Configuration
- `LABEL <entry title>`, `KERNEL <kernel image>`, `INITRD <ramdisk image>`, `APPEND <kernel parameters>`

#### Features missing from Grub:
- memdisk (in Grub2/Legacy use SysLinux's binary: `linux16 memdisk [iso] ; initrd16 <imagefile>`)
	- https://www.syslinux.org/wiki/index.php?title=MEMDISK



## FreeBSD bootloader
- https://www.freebsd.org/doc/handbook/boot-introduction.html
- https://wiki.freebsd.org/ia64/BootLoader



## Haiku (BeOS) bootloader
- https://www.haiku-os.org/docs/userguide/en/applications/bootmanager.html
- https://www.haiku-os.org/docs/userguide/en/bootloader.html
- https://github.com/haiku/haiku/blob/master/src/apps/bootmanager/bootman.S



# Historical

## Grub Legacy
Still in use for a few features not ported to Grub2? What are these?
- menu.lst config is almost the same, but conversion is still a barrier



## Grub4Dos
Features not in Grub2:
- Disk emulation (loop device) through Bios Int 13h
	- `map <imagefile> (hd8) ; map --hook ; root (hd8) ; chainloader (hd8)`
	- http://diddy.boot-land.net/grub4dos/files/map.htm#hd32
- In Grub2 it would be: `loopback loop0 <imagefile>`
	- https://www.gnu.org/software/grub/manual/grub/html_node/loopback.html
	- Todo: hook 13h, use first available drive number, pass in %DL
- Memdisk: add `--mem`
	- `map --mem <imagefile> (fd0) ; map --hook ; root (fd0) ; chainloader (fd0)`



## eLiLo
<small>(honouring the memory of (e)LiLo)</small>

- Only for UEFI systems. Tiny.
- Kernel and initramdisk has to be EFI accessible: on the EFI System Partition, or any FAT filesystem, or using extra EFI filesystem drivers.
- Bare-bones text-mode prompt, boot entries not listed.
- Orphaned: Debian dropped it in 2014, and RH & SUSE stopped using this tree.
- Only loads linux, no other OS. Launches the kernel the traditional way, not using EFI stub.

#### Configuration: /etc/elilo.conf
- Same as LiLo, similar to SysLinux.
- `image=<kernel image>`, `label=<entry title>`, `initrd=<ramdisk image>`, `append=<kernel parameters>`
- https://www.rodsbooks.com/efi-bootloaders/elilo.html
- https://sourceforge.net/projects/elilo/



# Configuration tools

## efibootmgr
- Configures the UEFI boot manager by setting boot entries in NVRAM.
- Not a bootmanager or bootloader in itself.
- https://github.com/rhboot/efibootmgr
- https://linux.die.net/man/8/efibootmgr



## kernelstub
- Copies kernel images to the EFI System Partition for bootmanagers to auto-detect them.
- https://github.com/isantop/kernelstub



## Boot Environments on ZFS filesystems
- https://github.com/vermaden/beadm  -  FreeBSD
- https://www.freebsd.org/cgi/man.cgi?query=beadm
- https://illumos.org/man/1m/beadm
- https://docs.oracle.com/cd/E26502_01/html/E29031/beadm-1m.html  -  Solaris



# Purpose-built, specialized bootloaders

## alexfru/BootProg
- 1 sector sized stage 1 loader for stage2 file stored in FAT filesystem
- https://github.com/alexfru/BootProg
- https://github.com/alexfru/BootProg/blob/master/boot32.asm
- https://github.com/alexfru/BootProg/blob/master/boot16.asm



