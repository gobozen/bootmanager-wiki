---
title: GPT on partitionless disks
---

How to create a GPT for a partition-less disk?

Partition-less (aka. superfloppy) disks lack an MBR / GPT / APM or another recognized structure containing a crucial information: what system is responsible for (owns) which part of the disk. Many Operating Systems and tools don't know how to handle such disks. Even worse, some of them try to "fix" those disks they don't understand. This risk could be mitigated with a "protective GPT" representing the disk as a single, full-disk partition. Let's explore how the GPT specification can include this use-case.

Isohybrid disk images demonstrate in practice how a whole-disk partition can be represented in GPT. To be bootable from CD and USB flash drive as well, these images contain a GPT, an MBR, and an EFI System Partition *inside* the Iso9660 filesystem. This hybrid, non-compliant configuration is understood and accepted by the boot process of BIOS and UEFI firmwares, whether it's read from a CD or an USB flash drive. That's 4 different boot processes.

The Guid partition table as currently specified has the necessary fields to represent such configuration, but some values are out of the valid range defined by the UEFI specification. This document explores how to define the limits of GPT - without changing the format - to allow such setups, and how tools should interpret a GPT with such unexpected values. The purpose is to construct a GPT which is accepted by operating systems, and preserved - not destroyed - by tools.

The best behaviour from current tools that follow the limits of the current specification is to present such partition table to the user, likely accompanied by a warning that it's an invalid configuration. The advanced user creating such configuration most likely understands this as a limitation of the tool, and knows how to handle it without damage. Any tool or OS making changes without user request to such non-compliant setups has to be avoided (it's bad behaviour anyway). In the future this document will be extended with reports on how operating systems and tools react to these configurations.


##### Contents
<!-- MarkdownTOC levels="1,2,3" autolink="true" -->

- [Full-disk partition?](#full-disk-partition)
- [Protecting the GPT from the containing filesystem](#protecting-the-gpt-from-the-containing-filesystem)
- [Filesystems used in partition-less setups](#filesystems-used-in-partition-less-setups)
	- [BtrFS](#btrfs)
	- [IsoHybrid](#isohybrid)
	- [Ext2+](#ext2)
	- [Fat32](#fat32)
- [Overview of a "protective" GPT](#overview-of-a-protective-gpt)
	- [AlternateLBA](#alternatelba)
	- [PartitionEntryLBA](#partitionentrylba)
	- [FirstUsableLBA](#firstusablelba)
	- [NumberOfPartitionEntries](#numberofpartitionentries)
	- [Revision](#revision)
- [Optional extensions to GPT 1.0](#optional-extensions-to-gpt-10)
	- [BlockSize](#blocksize)
	- [Relocatable primary GPT header](#relocatable-primary-gpt-header)
	- [1-sector GPT](#1-sector-gpt)
	- [GPT inside a partition](#gpt-inside-a-partition)
- [References](#references)
- [Guides, forums](#guides-forums)
- [GPT specification and on-disk format](#gpt-specification-and-on-disk-format)

<!-- /MarkdownTOC -->



## Full-disk partition?

A partition-less disk is basically a full-disk partition, covering the entirety of the disk, from sector 0 to the last. Think of it as a cake "cut" into one big slice: no cuts made at all. In this case the GPT is the tiny card stating for passersby that this cake is taken, do not touch. This is to prevent "helpful" actions to fix the partitioning, that happened in the past, eg. WinXP overwriting partitions when it couldn't understand the partition table.



## Protecting the GPT from the containing filesystem

Where to put that card without damaging the cake? It will be inside the partition, therefore it has to be protected from the filesystem overwriting it.

This might be as simple as Btrfs reserving a 64 KiB System Area from sector 0, where the GPT can reside, or as complex as immutable file(s) mapped to the exact region used by the GPT, then hidden from the user.

- Sector 1 used by the GPT header is in a reserved area in many filesystems, therefore protected and available. If it is not available, then either the FS or GPT has to relocate that sector. This requires changing tools or drivers, huge effort with big risks. LVM is one system that has a collision here.
- Only one sector will be allocated for GPT entries. By default it is sector 2. If it's not available, then it can be moved to another reserved sector, or an immutable file allocation.

- The secondary GPT also has to be reserved or protected by the FS on a full-disk partition. The simplest solution is to set the filesystem's size field to `GPT2.PartitionEntryLBA` (or last sector to `GPT2.PartitionEntryLBA - 1`).
- If it has to be part of the filesystem's managed area, it should be protected by an immutable file allocation. If a file cannot be allocated over the last sector(s), then allocate over just the entries, these can be relocated as necessary. The GPT2Header should be on the last sector, if available, otherwise at the end of the file allocated for the GPT2Entries.

- The file(s) allocated over the GPT entries (or header) will be protected with the immutable attribute. [Design and Implementation of the Second Extended Filesystem](https://www.nongnu.org/ext2-doc/ext2.html#EXT2-IMMUTABLE-FL) says "The blocks associated with the file will not be exchanged. If for any reason a file system defragmentation is launched, such files will not be moved. Mostly used for stage2 and stage1.5 boot loaders." Other filesystems ( [BtrFs](https://btrfs.wiki.kernel.org/index.php/Manpage/btrfs(5)) ) and chattr documentation are not so exact. Reading the filesystem code might be necessary to be sure.
	- Linux: `chattr +i  $file`
	- Bsd: `chflags simmutable hidden  $file`
	- Fat32, Ntfs: system attribute
- A tool creating this configuration can use any other feature that is available in the FS.

- The protective file(s) can be visible to the user, or hidden in a separate subvolume, or hidden by a mount point.
- How to name these files is up to discussion. Something along the lines /dev/disk/GPT1Entries, GPT2Raw (header+entries) or GPT2Entries. On a root partition these are hidden by the /dev mount.



## Filesystems used in partition-less setups

Let's start with recent filesystems introduced around and after the turn of the century, with new features such as volume-management integrated, enabling them to be used in place of partitioning.

- [BtrFS](#btrfs) (Linux): the popular Linux filesystem is well prepared and commonly used for partition-less.
- XFS (Linux), Stratis (RedHat): Superblocks are in sector 0 of each Allocation Group (a slice of the partition). The first AG seems to start at the beginning of the partition. I have not found proper information about this.
- (Open)ZFS (Solaris, FreeBSD, Linux?): 8KiB is reserved in the first slice for Volume Table of Contents (similar to GPT). This space is suitable for the primary GPT.
- LVM (not an FS): Sector 1 (or any of sector 0-3?) of a Physical Volume contains the LVM Disk Label. If a full disk is made into a Physical Volume, then the LVM Disk Label occupies the same sector as the GPT header would. A "protective" GPT is possible only with [Relocation](#relocation).
- [IsoHybrid](#isohybrid) (CD, DVD, USB): Although not capable of volume management, it is a clever combination of different partitioning and booting schemes inside an Iso9660 filesystem.
- [Fat32](#fat32) (USB): Used on USB Flash Drives for compatibility. The partition-less "superfloppy" setup has some challenges.


### BtrFS
- Luckily the foresighted developers of Btrfs put the first superblock at the 64KiB address, making it an easy candidate for this hybrid.
- The first GPT has space to stay at the standard sector 1 and following sectors. In this setup we relax the hard-minimum of 16 KiB for the entries and use 1 sector for the header and 1 for the entry(ies). Moving the entry to the end of the header sector would be a structural change, breaking the ability of current tools to read it, which we do not want.
- The secondary copy of the GPT at the end of the disk could collide with the backup superblocks at 64 MiB and 256 GiB in the rare case when the disk's size is exactly (64 MiB or 256 GiB) + (1 or 2 sectors). In this case Btrfs is on the edge: it has one less superblock if the disk is smaller with 2 sectors. To avoid further complication of relocating the superblock the 3rd / 2nd superblock is dropped by making the file system 2 sectors smaller than the disk.


### IsoHybrid
- Has 32 KiB unused "System Area" at the beginning, that contains an MBR and a GPT. The backup is also present at the end of the disk.
The MBR and GPT both declare 2 partitions:
- 1st partition for the whole disk (containing the Iso9660 file system). The Iso9660 partition must start on sector 0 for the sector offsets to be valid in the filesystem.
- 2nd partition for the EFI System Partition, stored "inside" the Iso9660 file system. This contains the 32-bit and 64-bit PEs for UEFI boot.
- Sector 0 contains the boot binary (before the MBR) for booting as an USB-HDD (or USB-FDD) on BIOS firmware.
The El Torito extension (CD Boot Record at 0x8800, pointing to El Torito Catalog at 0x10800) declares:
- a 80x86 boot image for booting from CD on BIOS firmware,
- and the EFI System Partition as UEFI boot image for booting from CD on UEFI firmware.
The icing on the cake is the subpartition for an EFI System Partition inside the Iso9660 partition.


### Ext2+
- Has 1 KiB free space before first superblock. With 512B sectors the GPT1Header can be at the expected sector 1.
- GPT1Entries has to be relocated, protected by an immutable file allocation, and `GPT1.PartitionEntryLBA` adjusted.
- With 4 KiB sectors the superblock is in sector 0 (if the implementation is true to the documentation) and sector 1-2 can be allocated to the GPT, protected by an immutable file allocation.


### Fat32
- Has FS Information Sector in sector 1 by default. This can be relocated: the EBPB field at sector 0 offset 0x30 stores the sector number.
- The GPT1Entries replace the boot code in sector 2, therefore the original boot loader won't be usable, but a custom-written one can fit in the rest of the reserved sectors (offset 0xE, usually 32).



## Overview of a "protective" GPT

Relaxing some limits of the GPT specification allows us to create a GPT that properly defines a full-disk partition, making it understandable and compatible for tools and OSes.

### AlternateLBA
- `GPT1.AlternateLBA` can be different from the disk size. If its smaller, there are sectors after the secondary GPT, these can be allocated by a partition entry.
- If it is larger than the disk size, the virtual disk was shrinked, and lost the secondary GPT. Tools can offer the user to recreate it, if there is unallocated space at the new end of the disk or between partitions.


### PartitionEntryLBA
- `GPT1.PartitionEntryLBA` can be any value (by default it is `GPT1.MyLBA + 1`). It has to be allocated elsewhere, if that sector collides with a filesystem structure.
- Fileystems usually have a "System Area" at the start. Some of them can contain the primary GPT without extra effort. See [GPT inside a partition](#gpt-inside-a-partition) for details.
- `GPT2.PartitionEntryLBA` can be any value as well (by default: `GPT2.MyLBA - PartitionEntryBlocks`).
- Missing from header: `PartitionEntryBlocks = (NumberOfPartitionEntries*SizeOfPartitionEntry-1) / BlockSize + 1` (-1 +1 trickery rounds *up* the division)


### FirstUsableLBA
- `FirstUsableLBA` can be `<= GPT1.MyLBA`, it is 0 in this setup. The first sector of a filesystem is the base for addressing every sector in it, therefore the distinguishing criteria of a partition-less setup is
- `LastUsableLBA` can be `>= GPT1.AlternateLBA`. By default it is the value originally specified: `GPT2.PartitionEntryLBA - 1`. Choosing the last sector is not as critical as the first. Only if necessary should the secondary GPT be considered part of the main partition. In this case the filesystem has to be constructed so it protects or reserves those sectors.


### NumberOfPartitionEntries
- The rule of minimum 16 KiB space for entries is lifted, but it remains the default for standard configurations.
- Only one sector is reserved for entries. With the default `SizeOfPartitionEntry == 128` this allows for 4 entries on 512B logical sectors, or 32 entries on 4 KiB sectors.
- The `NumberOfPartitionEntries` field is set to a value between 1 and the number of entries that fit in one sector. By default it is set to 1 + the number of subpartitions (see later).
- There is one entry (not necessarily the first) that represents the whole disk as a partition. `StartingLBA == 0`, `EndingLBA == LastUsableLBA`. Different use-cases can divert from this recommendation.


### Revision
- The `Revision` field should possibly be increased to 0x00010001 (rev. 1.1), to tell current tools this GPT holds unexpected, but correct values.



## Optional extensions to GPT 1.0

### BlockSize
- GPT is missing the `BlockSize` field: the sector size used in creation of the table and partitions (== the number of bytes between LBA 0 and 1, or LBA N and N+1). When this is necessary: the same binary image (bytestream) can be replicated (or backed up) on different mediums with different logical sector size. Eg. an advanced format drive with 4 KiB sector size backed up to an old drive with 512B sectors, or an ISO image created with 2 KiB sector size can be replicated on USB flash drive with 512B sector size (the GPT in ISO images is constructed with 512B logical sector size for this reason).
- The `BlockSize` field can be declared as UINT32 at offset 92, where the rev. 1.0 header ends -> `HeaderSize = 96`.
- This is not necessary for a partition-less disk, but for backups and replicas.
- This is a structural change compatible with GPT 1.0. Tools don't need to be made aware of it as long as they respect the increased `HeaderSize` and retain the additional field.


### Relocatable primary GPT header
- Sector 1 can get crowded. It's in use by LVM Disk Label, also Fat32 FS Information Sector is there by default.
- By allowing `GPT1.MyLBA != 1` these systems can be made compatible, capable of co-existing.
- If the GPT1Header is not in sector 1, this would require firmwares and tools to scan for the "EFI PART" signature in a small area at the start of a disk, up to eg. 64 KiB (where the Btrfs superblock is). Once the GPT1Header is found, firmwares can save the sector number to use on the next bootup, and rescan if it was (re)moved. The UEFI spec skipped an opportunity for greater compatibility by hardwiring `GPT1.MyLBA`.
- Note: `GPT1.MyLBA == 0` means the GPT header is replacing the Master Boot Record. In a pure UEFI environment there is no need for it.


### 1-sector GPT
- The GPT header has enough space left for 3 entries at offset 128, still leaving 128-92 = 36 bytes reserved.
- The benefit is only one sector has to be protected for each GPT copy in the filesystem. `GPT1.PartitionEntryLBA == GPT1.MyLBA`, `GPT2.PartitionEntryLBA == GPT2.MyLBA`, `NumberOfPartitionEntries <= 3`.
- This is an incompatible structural change - it requires an additional branch in tools -, therefore its benefits outweigh its costs, but still a possibility if a more beneficial use-case comes up. Considerable risk: unaware tools might write the entries over the header. `PartitionEntryLBA == 0` might be a safer representation.
- Alternative: GPT1Entries detached from GPT1Header does not need structural change, but has the added complexity of protecting the GPT1Entries by an immutable file allocation.
- Ext2+ is the only FS I found that has a collision with the GPT1Entries: it keeps its superblock on sector 2 (only with 512B sector size).


### GPT inside a partition
For tools and specialized use-cases: How to interpret a GPT inside a partition, further dividing it?
- Take the absolute LBA of GPT1Header as `GPT1RealLBA`. Calculate `LBAOffset = GPT1RealLBA - GPT1.MyLBA`
- Find the secondary GPT at `GPT2RealLBA = GPT1.AlternateLBA + LBAOffset`.
- If it's there then check the validity of both GPTs with all stored LBA values offsetted by `LBAOffset`.
- If valid then it can be interpreted as a disk image starting at `LBAOffset`.



## References

- https://btrfs.wiki.kernel.org/index.php/On-disk_Format#Superblock
- http://www.giis.co.in/Zfs_ondiskformat.pdf
- https://www.nongnu.org/ext2-doc/ext2.html#DEF-SUPERBLOCK


## Guides, forums

- https://wiki.archlinux.org/index.php/Btrfs#Partitionless_Btrfs_disk
- https://wiki.archlinux.org/index.php/GRUB/Tips_and_tricks#Install_to_partition_or_partitionless_disk
- https://unix.stackexchange.com/questions/14010/the-merits-of-a-partitionless-filesystem#19851
- http://www.kernel-overload.com/partition-less-root-directory-centos-7/
- https://linuxhint.com/zfs_vs_xfs/


## GPT specification and on-disk format

- [UEFI Specification Version 2.7 (Errata A) (2017 September 6)](http://www.uefi.org/sites/default/files/resources/UEFI%20Spec%202_7_A%20Sept%206.pdf)
	Section 5.3.2 GPT Header


