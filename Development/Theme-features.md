---
title: Theme features
---

The bootmanager - although visible for a short time - gives the first impression about a system, a lasting impression for the end-user. In professional products, it is part of the brand experience. This document collects a few commonly used UI elements that enable high-quality user experience in this restricted environment.

##### Contents
<!-- MarkdownTOC levels="1,2" autolink="true" -->

- [Targets of development:](#targets-of-development)
  - [Layout](#layout)
  - [Transitions when showing/hiding bootmanager screen](#transitions-when-showinghiding-bootmanager-screen)
  - [Transitions when selecting a menuitem](#transitions-when-selecting-a-menuitem)
- [Css features to implement](#css-features-to-implement)
- [Resources](#resources)

<!-- /MarkdownTOC -->



# Targets of development:
The purpose is to increase user satisfaction with attractive UI themes, and to increase user involvement by lowering the barrier to entry for theming. Reusing widely-adopted elements of css will enable people familiar with webdesign to hack on themes. Syntax-mapping of theme Dom to Html makes it possible to design and try the theme in a browser, if care is taken to only use this limited subset of css.


## Layout
- Basic container for list of entries / tools:  `display: grid`
- Popup, overlay:  `panel { position: absolute ; position-x: center ; position-y: top 20% ; width: 40em ; height: 10em }`
  - Short: `panel { position: absolute center top 20% ; size: 40em 10em }`
- Frames: `border, border-radius, box-shadow`
- `box-sizing: content-box padding-border-in-margin`
  - `border-top-width: var(--border-top-width) ; padding-top: var(--padding-top)`
  - `margin-top: calc(var(--margin-top) - var(--padding-top) - var(--border-top-width))`
  - Can't be expressed with css var: padding, border might overflow margin (independently of overflow setting).


## Transitions when showing/hiding bootmanager screen
- Fade in/out -> `display { transition: opacity 0.5s linear } ; display.outofview { opacity: 0 }`
  - Screen is constructed hidden, then class is removed immediately to trigger fadein transition. Hide and fade out when booting: `display.outofview.booting`
  - Built into menu module, not part of the theme. `screen`s defined by theme are the children of `display`.
  - If gfxmode==keep then the background stays onscreen -> `display.booting` :not(.outofview) -> all screens `.outofview`
  - `display` === `window`, `screen` === page aka. `document`, `panel` === `div`
- Slide in/out -> `screen { transition: transform 0.3s ease-out } ; screen.outofview { transform: translateY(100%) ; transition: transform 0.2s ease-in }`
  - All screens have `.outofview` class initially, `screen#main` is shown first by removing this class.
  - https://css-tricks.com/ease-out-in-ease-in-out/
  - Following best practice in css: avoid re-layout when animating position `{left/top}=100%`, animate `transform:translate{X/Y}(100%)` instead.
  - Partitioning the theme: one file per screen and major panels. Panels, elements can be cross-referenced with id.


## Transitions when selecting a menuitem
- Background opacity: `menuitem { transition: background-opacity ; background-color: black ; background-opacity: 0 } ; menuitem:focus { background-opacity: 0.4 }`
- Grow/shrink upwards: `menuitem { transition: transform 1s ease-in-out ; transform-origin: bottom center } ; menuitem:focus { transform: scale(1.2) }`
- Grow more, move unfocused menuitems: `... menuitem.before-focus { transform: translateX(-50%) } ; menuitem.after-focus { transform: translateX(50%) } ; menuitem:focus { transform: scale(2) }`
- Alternative, if there is a focused menuitem at all times: `... menuitem:focus { transform: translateX(50%) scale(2) ; menuitem.after-focus { transform: translateX(100%) } }`
- Or keep focused item in center, step the elements through fixed positions. Sounds interesting for css. Pseudo-css, relativeIndex=index from focused element:
  `menuitem { position: absolute center ; transition: transform 1s ease } ; menuitem:focus { transform: scale(2) } ; menuitem { transform: translateX(calc( 100%*(var(relativeIndex)+sign(var(relativeIndex))) )) }`
- Next step: How to map content elements to placeholder elements in html/css, and animate moving content from one placeholder to another? How to animate the position: fixed x,y coordinates?



# Css features to implement
- `position: absolute, top, left, height, width`
  - Optional custom properties: `position-x`: left/center/right +-20%, `position-y`: top/center/bottom +-10em
- `padding, border, margin`
- `border-radius, box-shadow`
- `display: grid`
  - `grid-template-columns`: auto
  - `grid-template-rows`: repeat(3, 100px)
  - `grid-gap`: 1em
- `background`
  - `background-image`
  - `background-opacity`
  - `background-color`
- `opacity`
- `transform`: scale, translate, rotate (secondary)
- `transition`: [transition-property] [transition-duration] [transition-timing-function] [transition-delay]
  - `transition-property`: none, all, opacity, transform
  - `transition-timing-function`: ease, ease-out, ease-in, ease-in-out, linear, cubic-bezier
  - `transition-duration`
  - `transition-delay`: not necessary until there is a use-case for it
- `var(), calc()`: reactive, pure functional scripting



# Resources
- https://css-tricks.com/snippets/css/complete-guide-grid/
- https://css-tricks.com/almanac/properties/t/transition/
- https://developer.mozilla.org/en-US/docs/Web/CSS/transition
- https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Transitions/Using_CSS_transitions
- https://css-tricks.com/almanac/properties/t/transform/
- https://developer.mozilla.org/en-US/docs/Web/CSS/transform
- https://www.html5rocks.com/en/tutorials/speed/high-performance-animations/

