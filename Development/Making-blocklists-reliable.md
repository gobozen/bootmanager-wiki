---
title: Making blocklists reliable
---

On BIOS systems Stage 1 (the bootsector) and Stage 2 (first sector of `core.img`) contain blocklists (absolute LBA sector index) to locate the next stage to load. Relocating `core.img` on the drive will break the boot process when the actual sectors get overwritten (this can happen at any time after the relocation). This document investigates what can cause a relocation and how to avoid, or adapt to this change.

##### Contents
<!-- MarkdownTOC levels="1,2" autolink="true" -->

- [Making files non-movable](#making-files-non-movable)
	- [On Fat fs \(revived by the EFI System Partition\)](#on-fat-fs-revived-by-the-efi-system-partition)
	- [Common Linux filesystems: Btrfs, ext4, Reiser, ...](#common-linux-filesystems-btrfs-ext4-reiser-)
	- [Bsd filesystems: Zfs, ...](#bsd-filesystems-zfs-)
- [Adapting to relocation](#adapting-to-relocation)
	- [Moving partition with gparted](#moving-partition-with-gparted)
	- [Defragmenting/relocating file on disk](#defragmentingrelocating-file-on-disk)

<!-- /MarkdownTOC -->



# Making files non-movable

## On Fat fs (revived by the EFI System Partition)
- Fat fs has the system attribute to protect the bootloader and the pagefile (swap). This attribute prohibits relocation of the files.
- Copy core.img to esp:/grub/i386-pc/core.img, add system attribute.
- Blocklist is reliable as long as partition is not moved (for ex. by gparted), or a misbehaving defragmenter ignores the system flag.


## Common Linux filesystems: Btrfs, ext4, Reiser, ...
- Linux chattr has the following attributes:
```
e: extent format: The e attribute indicates that the file is using extents for mapping the blocks on disk.
i: immutable: It cannot be deleted or renamed, no link can be created to this file and no data can be written to the file.
t: no tail-merging: For those filesystems which support tail-merging, a file with the t attribute will not have a partial block fragment at the end of the file merged with other files.
```


## Bsd filesystems: Zfs, ...
- Bsd chflags has System immutable flag (schg, schange, simmutable): File cannot be changed, renamed, moved, or removed



# Adapting to relocation

## Moving partition with gparted

The file location in the partition (logical sector number relative to partition start sector remains the same). Happens seldom, but then it invalidates the absolute sector numbers stored in Grub bootsector (boot.S) and first sector of core.img (diskboot.S). The absolute sector number is increased/decreased by the offset the partition was moved.

To determine this: store the partition start LBA (same as in the MBR/GPT partition table) when installing. Upon boot check if the partition start is still the same, if not:  image_sector += part_start_current - part_start_when_installing. Warn the user to do a fresh grub_install.

Storing logical sector number instead of absolute is an option (implementation detail). Hexediting by hand the absolute LBA might be easier than the logical (need to add/subtract the partition start).

Storing the partition start gives reliability in case the partition table is corrupted or changed in unanticipated way:
- Partition number changes (entries are reordered). Partition can be searched for by starting sector (or uuid in a GPT). This has to fit into the little space left in the 512 byte bootsector.


## Defragmenting/relocating file on disk

If filesystem attributes are disregarded and the blocklisted file is relocated, have one or two redundant core.img copies in another partition, or embedded between partitions, also referenced by blocklist. This gives redundancy even if those locations are just as vulnerable to relocation (in partition) or overwrite (unprotected outside partition).



