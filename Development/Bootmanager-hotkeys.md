---
title: Bootmanager hotkeys
---

## Hotkeys used by UEFI / BIOS boot menu

- Big list: https://kb.wisc.edu/page.php?id=58779
- A few by vendor: https://123qweasd.com/blog/tech/how-to-boot-from-flash-drive-ultimate-guide/#attachment_291
  - section: Step 1: Turn on or restart PC -> Boot Menu hotkey table by vendor
  - Usually, for entering in settings you need to use next hotkeys: DEL, F2, F12.
  - To run the Boot Menu try to press: F8, F9, F10.



