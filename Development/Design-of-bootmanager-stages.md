---
title: Design of bootmanager stages
---

##### Contents
<!-- MarkdownTOC levels="1,2" autolink="true" -->

- [Stage 0 - MBR - boot.img](#stage-0---mbr---bootimg)
	- [Feature options](#feature-options)
- [Stage 1 - PBR](#stage-1---pbr)
	- [Filesystem support](#filesystem-support)
	- [Loading strategies](#loading-strategies)
	- [Pinning files](#pinning-files)
	- [Compatibility options](#compatibility-options)
- [Stage 2 - core.img](#stage-2---coreimg)

<!-- /MarkdownTOC -->



# Stage 0 - MBR - boot.img

## Feature options

- Read GPT partition table -> `OPTION_READ_GPT`
	- Load active partition
	- Load Grub partition: skip stage 1 (type guid:  "Hah!IdontNeedEFI"  -  middle-endian `21686148-6449-6E6F-744E-656564454649`)
- Read MBR partition table -> `OPTION_READ_MBR`
	- Load active partition
	- Current: Load Stage 1 from partition gap -> `OPTION_LOAD_STAGE1_FROM_LBA`
		- 64 bit LBA, 1 sector
		- Should pass on sector number instead of burning absolute LBA into stage 1.
	- Load Stage 2 from partition gap -> `OPTION_LOAD_STAGE2_FROM_LBA`
		- 48 bit LBA + 16 bit sector count (max. 32 MiB contiguous block) stored at 0x1b0 (before disk UID / NT_MAGIC, as for isohybrid HYBRID_BOOT).
- Use non-extended Int 13h if extended is not available (Bios before ca. 1998) -> `SUPPORT_BIOS_PRE1998`
- Print stage messages -> `OPTION_PRINT_STAGE`
- 4 KiB sector variant -> `BUILD_4K_SECTOR`
	- Around 2014 there were Bios motherboards used with Advanced Format drives.
	- Gpt, Mbr, Stage2 reading all fits + partition selector + descriptive error messages.



# Stage 1 - PBR
- Same purpose as diskboot.img + installed in the PBR.

## Filesystem support

- Placing boot code in the first sector of the partition has different constraints on different filesystems. Fat and Ntfs stores the BPB in sector 0, Fat32 the FS Info Sector in sector 1, Ext2+ has 1 KiB free (2 sectors), Btrfs has 64 KiB free.
- `BUILD_FOR_FS` = GRUB (no FS) / EXTFS (ext2-4) / BTRFS / ZFS / LVM? / RAID? / NTFS (with BPB) / FAT (with BPB) / ...
- Ext2+: 1 KiB unused boot block.
- Ntfs: 16 sectors for the boot sector and the bootstrap code. Bpb at 0x0b..0x54.
- Fat:
	- At sector 0 offset 0x0e, 2 bytes: Count of reserved logical sectors. The number of logical sectors before the first FAT in the file system image. At least 1 for this sector, usually 32 for FAT32 (to hold the extended boot sector, FS info sector and backup boot sectors).
- Fat32 only:
	- Bpb at 0x0b..0x5a.
	- Offset 0x30, 2 bytes: Logical sector number of FS Information Sector, typically 1, i.e., the second of the three FAT32 boot sectors. File system implementations should never attempt to use logical sector 0 as FS Information sector and instead assume that the feature is unsupported on that particular volume.
	- Offset 0x32, 2 bytes: First logical sector number of a copy of the three FAT32 boot sectors, typically 6. Values of 0x0000 and/or 0xFFFF are reserved and indicate that no backup sector is available.

## Loading strategies

- Current: load absolute LBA blocklist. Very fragile: moving partition, defragmenting files changes the LBA, making Grub unable to reach even the Rescue prompt.
- Any FS: load partition relative blocklist of pinned down / stickied files (immutable / system attribute). Safe if the fs drivers and defrag respect the flag. Btrfs?
- BIOS boot partition (BUILD_FOR_FS = GRUB): load sectors +0 ..< +length
	- Stage 1 (sector 0) contains length (u8 sector count 128KiB / u16 sector count 32MiB / u32 byte count 4GiB) of core, load address? (u16 CS / u32 linear address), entry point? (u16 IP relative to load address / u16:u16 CS:IP)
- FAT32 read implemented in 16-bit asm fits 1-2 sectors. Good alternative for Efi System Partition.

## Pinning files

- Fat, Ntfs:  s (System). Indicates that the file belongs to the system and must not be physically moved (e.g., during defragmentation), because there may be references into the file using absolute addressing bypassing the file system (boot loaders, kernel images, swap files, extended attributes, etc.).
- Ext2-4, Btrfs?, ...:  i (Immutable File / [EXT2_IMMUTABLE_FL](http://www.nongnu.org/ext2-doc/ext2.html#EXT2-IMMUTABLE-FL)). The blocks associated with the file will not be exchanged. If for any reason a file system defragmentation is launched, such files will not be moved. Mostly used for stage2 and stage1.5 boot loaders.

## Compatibility options

- Use non-extended Int 13h if extended is not available (Bios before ca. 1998) -> `SUPPORT_BIOS_PRE1998`
- Load from floppy: do CHS probing (very old hardware) -> `SUPPORT_FLOPPY`



# Stage 2 - core.img

- Treated as a partition on its own, no filesystem, just the contents of core.img.
- 1st sector is built by `BUILD_FOR_FS = GRUB`: a stage 1 PBR without BPB or partition table, 512 bytes available for code (minus 2 byte for 0x55 0xAA signature).
- 2nd sector contains list of optional modules (rescue, video, menu, last: empty): 2 byte start sector (32MiB addressable) + 6 byte name / 4 start + 12 name / 6 start + 2 len + 8 name

