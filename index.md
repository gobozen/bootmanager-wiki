---
#title: Resources for bootmanager development
title: Index
layout: home
---

## RFC
- [GPT on partitionless disks](GPT-on-partitionless-disks)

## Bootmanagers in use
- [Bootmanager roundup 2018](Bootmanagers/Bootmanager-roundup-2018)
- [Booting ISO images](Bootmanagers/Booting-ISO-images)

## Grub 2 bootloader
- [GRUB boot sequence visualized](Grub/GRUB-boot-sequence-visualized)
- [GRUB community resources](Grub/GRUB-community-resources)
- [What type of kernel GRUB has](Grub/What-type-of-kernel-GRUB-has)
- [GRUB implementation notes](Grub/GRUB-implementation-notes)
- [Distributions' patches to Grub](Grub/GRUB-distribution-patches)

## Bootmanager development
- [Design of bootmanager stages](Development/Design-of-bootmanager-stages)
- [Making blocklists reliable](Development/Making-blocklists-reliable)
- [Theme features](Development/Theme-features)
- [Bootmanager hotkeys](Development/Bootmanager-hotkeys)

## Learning materials
- [Bootmanager stages](Learning/Bootmanager-stages)
- [BIOS boot sequence](Learning/BIOS-boot-sequence)
- [Asm resources](Learning/Asm-resources)
- [BIOS resources](Learning/BIOS-resources)

## Research
- [Bootmanager configuration file formats](Research/Bootmanager-configuration-file-formats)
- [Bootmanager theme formats](Research/Bootmanager-theme-formats)
- [Embedded scripting languages](Research/Embedded-scripting-languages)

## [Relevant specifications](Specifications/Relevant-specifications)


