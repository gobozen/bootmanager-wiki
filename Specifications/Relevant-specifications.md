---
title: Relevant specifications
---

##### Contents

<!-- MarkdownTOC levels="1,2" autolink="true" -->

- [Boot services](#boot-services)
	- [UEFI](#uefi)
	- [BIOS boot](#bios-boot)
	- [BIOS-UEFI hybrid MBR](#bios-uefi-hybrid-mbr)
	- [BIOS EDD specs](#bios-edd-specs)
	- [BIOS EDD drafts](#bios-edd-drafts)
- [Sparc, IEEE 1275 - Devicetree](#sparc-ieee-1275---devicetree)
- [Filesystems](#filesystems)

<!-- /MarkdownTOC -->



# Boot services

## UEFI
- [UEFI Specification Version 2.7 (Errata A) (2017 September 6)](http://www.uefi.org/sites/default/files/resources/UEFI%20Spec%202_7_A%20Sept%206.pdf)
- [List of UEFI specifications](https://uefi.org/specifications)
- [A Tour beyond BIOS Memory Map Design in UEFI BIOS](https://firmware.intel.com/sites/default/files/resources/A_Tour_Beyond_BIOS_Memory_Map_in%20UEFI_BIOS.pdf)


## BIOS boot
- [BIOS Boot Specification Version 1.01 (1996 January 11)](BIOS-Boot-Specification-specsbbs101.pdf)
	[[Source: specsbbs101.pdf]](http://www.scs.stanford.edu/05au-cs240c/lab/specsbbs101.pdf)
- [Bootable CD-ROM Format El-Torito Specification: (1995 January 25)](Bootable-CD-ROM-Format-El-Torito-specification-specscdrom.pdf)
	[[Source: specscdrom.pdf]](https://www.intel.com/content/dam/support/us/en/documents/motherboards/desktop/sb/specscdrom.pdf)
- [POST Memory Manager Specification Version 1.01 (1997 November 21)](POST-Memory-Manager-Specification-specspmm101.pdf)
	[[Source: specspmm101.pdf]](https://web.archive.org/web/20051111084304/http://www.phoenix.com/NR/rdonlyres/873A00CF-33AC-4775-B77E-08E7B9754993/0/specspmm101.pdf)


## BIOS-UEFI hybrid MBR
- [BIOS Enhanced Disk Drive-4 Hybrid MBR boot code annex Revision 3 (2010 January 4)](BIOS-EDD-4-Hybrid-MBR-Rev3-boot-code-annex-e09127r3.pdf)
	[[Source: e09127r3-EDD-4_Hybrid_MBR_boot_code_annex.pdf]](http://www.t13.org/Documents/UploadedDocuments/docs2010/e09127r3-EDD-4_Hybrid_MBR_boot_code_annex.pdf)
- [BIOS Enhanced Disk Drive-4 Hybrid MBR boot code annex Revision 2 (2009 November 6)](BIOS-EDD-4-Hybrid-MBR-Rev2-boot-code-annex-e09127r2.pdf)
	[[Source: e09127r2-EDD-4_Hybrid_MBR_boot_code_annex.pdf]](http://www.t13.org/Documents/UploadedDocuments/docs2009/e09127r2-EDD-4_Hybrid_MBR_boot_code_annex.pdf)
- [BIOS Enhanced Disk Drive-4 Hybrid MBR partition records annex Revision 1a (2009 November 6), split off from Revision 1](BIOS-EDD-4-Hybrid-MBR-Rev1a-partition-records-annex-e09150r0.pdf)
	[[Source: e09150r0-EDD-4_Hybrid_MBR_partition_records_annex.pdf]](http://www.t13.org/Documents/UploadedDocuments/docs2009/e09150r0-EDD-4_Hybrid_MBR_partition_records_annex.pdf)
- [BIOS Enhanced Disk Drive-4 Hybrid MBR support Revision 1 (2009 November 3)](BIOS-EDD-4-Hybrid-MBR-Rev1-support-e09127r1.pdf)
	[[Source: e09127r1-EDD-4_Hybrid_MBR_support.pdf]](http://www.t13.org/Documents/UploadedDocuments/docs2009/e09127r1-EDD-4_Hybrid_MBR_support.pdf)
- [BIOS Enhanced Disk Drive-4 Hybrid MBR support Revision 0 (2009 July 24)](BIOS-EDD-4-Hybrid-MBR-Rev0-support-e09127r0.pdf)
	[[Source: e09127r0-EDD-4_Hybrid_MBR_support.pdf]](http://www.t13.org/documents/UploadedDocuments/docs2009/e09127r0-EDD-4_Hybrid_MBR_support.pdf)
- [List of T13 Committee 2010 Documents](http://www.t13.org/Documents/MinutesDefault.aspx?year=2010&DocumentType=8) (download requires registration)
- [List of T13 Committee 2009 Documents](http://www.t13.org/Documents/MinutesDefault.aspx?year=2009&DocumentType=8)


## BIOS EDD specs
- [BIOS Enhanced Disk Drive Specification Version 4.0 Rev 4 (2010 December 14)](BIOS-Enhanced-Disk-Drive-Specification-v4-d2132r4.pdf) A.3 Hybrid MBR boot code
	[[Source: d2132r4-Enhanced\_Disk\_Drive\_-\_4\_EDD-4.pdf]](http://www.t13.org/Documents/UploadedDocuments/docs2010/d2132r4-Enhanced_Disk_Drive_-_4_EDD-4.pdf)
- [BIOS Enhanced Disk Drive Specification Version 3.0 Rev 0.9b (1998 June 24)](BIOS-Enhanced-Disk-Drive-Specification-v3-r09b-d1386r0-EDD.pdf)
	[[Source: d1386r0-EDD.pdf]](http://www.t13.org/Documents/UploadedDocuments/project/d1386r0-EDD.pdf)
- [BIOS Enhanced Disk Drive Specification Version 3.0 Rev 0.8 (1998 March 12)](BIOS-Enhanced-Disk-Drive-Specification-v3-r08-specsedd30.pdf)
	[[Source: specsedd30.pdf]](http://mbldr.sourceforge.net/specsedd30.pdf)


## BIOS EDD drafts
- [BIOS Enhanced Disk Drive Services 4.0 (2008 August 19)](BIOS-Enhanced-Disk-Drive-Services-v4-e08134r1.pdf)
	[[Source: e08134r1-BIOS_Enhanced_Disk_Drive_Services_4.0.pdf]](http://www.t13.org/Documents/UploadedDocuments/docs2008/e08134r1-BIOS_Enhanced_Disk_Drive_Services_4.0.pdf)
- [BIOS Enhanced Disk Drive Services - 3 D1572 Rev 3 (2004 November 5)](BIOS-Enhanced-Disk-Drive-Services-d1572r3-EDD3.pdf)
	[[Source: d1572r3-EDD3.pdf]](http://www.t13.org/Documents/UploadedDocuments/docs2004/d1572r3-EDD3.pdf)
- [BIOS BIOS Enhanced Disk Drive Services – 2 D1484 Rev 3 (2002 February 21)](BIOS-Enhanced-Disk-Drive-Services-d1484r3-EDD-2.pdf)
	[[Source: d1484r3-EDD-2.pdf]](http://www.t13.org/Documents/UploadedDocuments/project/d1484r3-EDD-2.pdf)
- [BIOS Enhanced Disk Drive Services D1386 Rev 5 (2000 September 28)](BIOS-Enhanced-Disk-Drive-Services-d1386r5-EDD.pdf)
	[[Source: d1386r5-EDD.pdf]](http://www.t13.org/Documents/UploadedDocuments/project/d1386r5-EDD.pdf)
- [BIOS Enhanced Disk Drive Services D1226 Rev 7 (1997 October 23)](BIOS-Enhanced-Disk-Drive-Services-d1226r7-Enhanced-BIOS.pdf)
	[[Source: d1226r7-Enhanced-BIOS.pdf]](http://www.t13.org/Documents/UploadedDocuments/project/d1226r7-Enhanced-BIOS.pdf)



# Sparc, IEEE 1275 - Devicetree

- [Devicetree Specification 0.2 (2017 December 20)](devicetree-specification-v0.2.pdf)
	[[Source: devicetree-specification-v0.2.pdf]](https://github.com/devicetree-org/devicetree-specification/releases/download/v0.2/devicetree-specification-v0.2.pdf)
- [Devicetree spec list v0.2](https://github.com/devicetree-org/devicetree-specification/releases/tag/v0.2)
- [Devicetree homepage](https://www.devicetree.org/)


# Filesystems

- [Ext2 at Non-Gnu](https://www.nongnu.org/ext2-doc/)
- [The Second Extended File System](https://www.nongnu.org/ext2-doc/ext2.html) [[Mirror]](https://www.win.tue.nl/~aeb/linux/fs/ext2/ext2.html)
- [Design and Implementation of the Second Extended Filesystem](http://e2fsprogs.sourceforge.net/ext2intro.html)  [[Mirror]](https://www.win.tue.nl/~aeb/linux/fs/ext2/ext2intro.html)


